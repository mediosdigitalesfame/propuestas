<?php include('contenidos/head.php'); ?>

	<body>
		
        <?php include('contenidos/header.php'); ?>

			<!--slider-->
			<section class="revolution_slider">
				<div class="r_slider">
					<ul>
						<li class="f_left" data-transition="cube" data-slotamount="7" data-custom-thumb="images/banner_2.jpg">
							<img src="images/banner_2.jpg" alt="" data-bgrepeat="no-repeat" data-bgfit="cover" data-bgposition="center center">
							 <div class="caption sfb stb color_light slider_title tt_uppercase t_align_c" data-x="center" data-y="246" data-speed="1000" data-easing="ease" data-start="2500"><b class="color_white">Bienvenido a la Web Oficial de Honda Altozano</b></div>
						</li>

						<li class="f_left" data-transition="cube" data-slotamount="7" data-custom-thumb="images/banner_1.jpg">
							<img src="images/banner_1.jpg" alt="" data-bgrepeat="no-repeat" data-bgfit="cover" data-bgposition="center center">
							<div class="caption lft ltt" data-x="center" data-y="58" data-speed="1500" data-start="1200" data-easing="easeOutBounce">
								<img src="images/slider_layer_img.png" alt="">
							</div>
							<div class="caption sfb stb color_light slider_title tt_uppercase t_align_c" data-x="center" data-y="246" data-speed="1000" data-easing="ease" data-start="2500"><b class="color_white">Promocion</b></div>
							<div class="caption sfb stb color_light" data-x="center" data-y="352" data-speed="1000" data-start="2900">
								<a href="#" role="button" class="button_type_4 bg_scheme_color color_light r_corners tt_uppercase">Ver Mas</a>
							</div>
						</li>
					</ul>
				</div>
			</section>
 
            <div class="page_content_offset">

			    <?php include('contenidos/botones.php'); ?>
 	 
		            <div class="container">

					<!--titulo vehiculos-->
					<div class="clearfix m_bottom_25 m_sm_bottom_20">
						<h2 class="tt_uppercase color_dark f_left heading2 animate_fade f_mxs_none m_mxs_bottom_15">Nuestros Vehiculos</h2>
						<div class="f_right clearfix nav_buttons_wrap animate_fade f_mxs_none">
							<button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left tr_delay_hover r_corners pb_prev"><i class="fa fa-angle-left"></i></button>
							<button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners pb_next"><i class="fa fa-angle-right"></i></button>
						</div>
					</div>
					<!--imagenes vehiculos-->
					<div class="product_brands m_sm_bottom_35 m_xs_bottom_0">
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a>
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a>  
					</div>
				</div>
			</div>

			<section class="bg_scheme_color call_to_action_2 m_bottom_45">
					<div class="container">
						<div class="d_table full_width cta_1 d_xs_block">
							<div class="d_table_cell v_align_m d_xs_block m_xs_bottom_30">
								<h1 class="color_light"><b class="f_size_ex_large">Contacta con Nosotros</b></h1>
							</div>
							<div class="d_table_cell v_align_m t_align_r d_xs_block t_xs_align_l">
								<button class="tr_delay_hover r_corners button_type_12 bg_light_color_2 color_dark f_size_large">Contactar!</button>
							</div>
						</div>
					</div>
				</section>


			<?php include('contenidos/footer.php'); ?>