<hr >
<br>
<div class="container">
	<!--botones-->
	<div class="row clearfix">
		<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
			<figure class="info_block_type_2 t_align_c">
				<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-thumbs-o-up d_inline_middle"></i></div>
				<h4 class="fw_medium color_dark m_bottom_15">COTIZA TU AUTO</h4>
			</figure>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
			<figure class="info_block_type_2 t_align_c">
				<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-expand d_inline_middle"></i></div>
				<h4 class="fw_medium color_dark m_bottom_15">CITA DE MANEJO</h4>
			</figure>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
			<figure class="info_block_type_2 t_align_c">
				<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-cogs d_inline_middle"></i></div>
				<h4 class="fw_medium color_dark m_bottom_15">CITA DE SERVICIO</h4>
			</figure>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
			<figure class="info_block_type_2 t_align_c">
				<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-wrench d_inline_middle"></i></div>
				<h4 class="fw_medium color_dark m_bottom_15">REFACCIONES</h4>
			</figure>
		</div>
	</div>
</div>
<hr >	 
<br>