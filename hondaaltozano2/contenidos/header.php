<!--boxed layout-->
		<div class="wide_layout relative w_xs_auto">
			<!--[if (lt IE 9) | IE 9]>
				<div style="background:#fff;padding:8px 0 10px;">
				<div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i class="fa fa-exclamation-triangle scheme_color f_left m_right_10" style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:16%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank" style="margin-bottom:2px;">Update Now!</a></div></div></div></div>
			<![endif]-->
			<!--markup header-->
			<header role="banner" class="type_5">
				<!--header top part-->
				<section class="h_top_part">
					<div class="container">
						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-5 t_xs_align_c">
								<ul class="d_inline_b horizontal_list clearfix f_size_small users_nav">
									<li><a href="#" class="default_t_color">Bienvenido</a></li>
									<li><a href="#" class="default_t_color" data-popup="#login_popup">Promocion</a></li>
									<li><a href="#" class="default_t_color" data-popup="#login_popup">Aviso Pago</a></li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-2 t_align_c t_xs_align_c">
								<p class="f_size_small">Llama a la Agencia: <b class="color_dark"> <i class="fa fa-phone"></i> (443) 340-4537</b></p>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-10 t_align_r t_xs_align_c">
								<ul class="horizontal_list clearfix d_inline_b t_align_l f_size_small site_settings type_2">
									<li class="container3d relative">
										<a role="button" href="#" class="color_dark" id="lang_button"><img class="d_inline_middle m_right_10" src="images/mini_honda_altozano.png" alt="">Honda Altozano</a>
										<ul class="dropdown_list type_2 top_arrow color_light">
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
										</ul>
										
									</li>
									<li class="m_left_20 relative container3d">
										<a role="button" href="#" class="color_dark" id="currency_button"><img class="d_inline_middle m_right_10" src="images/mini_grupo_fame.png" alt="">Grupo FAME</a>
										<ul class="dropdown_list type_2 top_arrow color_light">
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
										</ul>
										
									</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<!--header bottom part-->
				<section class="h_bot_part">
				<div class="menu_wrap">
						<div class="container">
							<div class="clearfix row">
								<div class="col-lg-2 t_md_align_c m_md_bottom_15">
									<a href="index.html" class="logo d_md_inline_b">
										<img src="images/logo.png" alt="">
									</a>
								</div>
								<div class="col-lg-10 clearfix t_sm_align_c">
									<div class="clearfix t_sm_align_l f_left f_sm_none relative s_form_wrap m_sm_bottom_15 p_xs_hr_0 m_xs_bottom_5">
										<!--button for responsive menu-->
										<button id="menu_button" class="r_corners centered_db d_none tr_all_hover d_xs_block m_xs_bottom_5">
											<span class="centered_db r_corners"></span>
											<span class="centered_db r_corners"></span>
											<span class="centered_db r_corners"></span>
										</button>
										<!--main menu-->
										<nav role="navigation" class="f_left f_xs_none d_xs_none m_right_35 m_md_right_30 m_sm_right_0">	
											<ul class="horizontal_list main_menu type_2 clearfix">
												<li class="current relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0">
													<a href="index.html" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Inicio</b></a>
												</li>
												<li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0">
													<a href="index_layout_wide.html" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Servicio</b></a>
													<!--sub menu-->
													<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
														<ul class="sub_menu">
															<li><a class="color_dark tr_delay_hover" href="#">Cita de Servicio</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Garantia Extendida</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Refacciones</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Club de Privilegios</a></li>
														</ul>
													</div>
												</li>
												 
												<li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0">
													<a href="category_grid.html" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Vehiculos</b></a>
													<!--sub menu-->
													<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
														<ul class="sub_menu">
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Insight</a></li>
														</ul>
													</div>
												</li>
												<li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0">
													<a href="category_grid.html" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Promociones</b></a>
													<!--sub menu-->
													<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
														<ul class="sub_menu">
															<li><a class="color_dark tr_delay_hover" href="#">Honda Finance</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Planes y Promociones</a></li>
															<li><a class="color_dark tr_delay_hover" href="#">Auto Financiamiento</a></li>
														</ul>
													</div>
												</li>
												<li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0">
													<a href="https://www.fameseminuevos.com" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Seminuevos</b></a>
												</li>
												<li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0">
													<a href="contacto.php" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Contacto</b></a></li>
											</ul>
										</nav>
										 
										 
									</div>
									 
								</div>
							</div>
						</div>
						<hr class="divider_type_6">
					</div>
				</section>
			</header>