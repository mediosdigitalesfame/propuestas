

			<!--markup footer-->
			<footer id="footer">
				 <div class="container">
						<div class="row clearfix">
							<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_20 m_xs_bottom_10">
								<!--social icons-->
								<ul class="clearfix horizontal_list social_icons">
									<li class="facebook m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Facebook</span>
										<a href="#" class="r_corners t_align_c tr_delay_hover f_size_ex_large">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="twitter m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Twitter</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="google_plus m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Google Plus</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-google-plus"></i>
										</a>
									</li>
									<li class="rss m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Rss</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-rss"></i>
										</a>
									</li>
									<li class="pinterest m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Pinterest</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-pinterest"></i>
										</a>
									</li>
									<li class="instagram m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Instagram</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-instagram"></i>
										</a>
									</li>
									<li class="linkedin m_bottom_5 m_left_5 m_sm_left_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">LinkedIn</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-linkedin"></i>
										</a>
									</li>
									<li class="vimeo m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Vimeo</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-vimeo-square"></i>
										</a>
									</li>
									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Youtube</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-youtube-play"></i>
										</a>
									</li>
									<li class="flickr m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Flickr</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-flickr"></i>
										</a>
									</li>
									<li class="envelope m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Contact Us</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-envelope-o"></i>
										</a>
									</li>

									
								</ul>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_20 m_xs_bottom_30">
								<h3 class="color_light_2 d_inline_middle m_md_bottom_15 d_xs_block">Notifame</h3>
								<form id="newsletter" class="d_inline_middle m_left_5 subscribe_form_2 m_md_left_0">
									<input type="email" placeholder="Tu correo electronico" class="r_corners f_size_medium w_sm_auto m_mxs_bottom_15" name="newsletter-email">
									<button type="submit" class="button_type_8 r_corners bg_scheme_color color_light tr_all_hover m_left_5 m_mxs_left_0">Suscribirse</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!--copyright part-->

<?php date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$anio=date('Y'); ?>


				<div class="footer_bottom_part">
					<div class="container clearfix t_mxs_align_c">
						<p class="f_left f_mxs_none m_mxs_bottom_10">&reg; <?php echo $anio; ?> <span class="color_light">Grupo FAME - Honda Altozano</span>. <i class="fa fa-user"> </i>  <a href="aviso.php" target="_self">Aviso de privacidad</a> <i class="fa fa-phone"></i> 01800 670 8386 </p>
						<ul class="f_right horizontal_list clearfix f_mxs_none d_mxs_inline_b">

							<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Honda Altozano</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i > <img src="images/mini_logo_honda.png" alt=""> </i>
										</a>
									</li>

									
						</ul>
					</div>
				</div>
			</footer>
		</div>
		<!--social widgets-->
		<ul class="social_widgets d_xs_none">
			<!--facebook-->
			<li class="relative">
				<button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Siguenos en Facebook</h3>
					<div id="fb-root"></div>
                    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2"></script>

					<div class="fb-page" data-href="https://www.facebook.com/grupofame" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/grupofame" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/grupofame">Facebook</a></blockquote></div>
				</div>
			</li>

			<!--twitter feed-->
			<li class="relative">
				<button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Latest Tweets</h3>
					<div class="twitterfeed m_bottom_25"></div>
					<a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color" href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
				</div>	
			</li>

			<!--contact info-->
			<li class="relative">
				<button class="sw_button t_align_c googlemap"><i class="fa fa-youtube-play"></i></button>
				 	<div class="sw_content">
					<h3 class="color_dark m_bottom_20">YouTube</h3>
					 
					<script src="https://apis.google.com/js/platform.js"></script>

                   <div class="g-ytsubscribe" data-channel="GrupoFameAutos" data-layout="default" data-count="hidden"></div>
				</div>
			</li>

			<!--twitter feed-->
			<li class="relative">
				<button class="sw_button t_align_c twitter"><i class="fa fa-bold"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Tweets</h3>
					<div class="twitterfeed m_bottom_25"></div>
					<a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color" href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
				</div>	
			</li>

			<!--contact form-->
			<li class="relative">
				<button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Contactanos</h3>
					<p class="f_size_medium m_bottom_15">Dejanos tus datos y en breve uno de nustros asesores te atendera.</p>
					<form id="contactform" class="mini">
						<input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name" placeholder="Nombre">
						<input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email" placeholder="Correo">
						<textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Mensaje" name="cf_message"></textarea>
						<button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">Enviar</button>
					</form>
				</div>	
			</li>
			<!--contact info-->
			<li class="relative">
				<button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Honda Altozano</h3>
					<ul class="c_info_list">
						<li class="m_bottom_10">
							<div class="clearfix m_bottom_15">
								<i class="fa fa-map-marker f_left color_dark"></i>
								<p class="contact_e">Paseo Altozano Zona Automotriz<br> Av. Montaña Monarca #1000</p>
							</div>
							<iframe class="r_corners full_width" id="gmap_mini" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.88754430981!2d-101.1644910846813!3d19.67480253798855!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d120a88d7dc89%3A0x945ba1aec77d55cf!2sHonda+Altozano+FAME!5e0!3m2!1ses!2smx!4v1551998396768"></iframe>

						</li>
						<li class="m_bottom_10">
							<div class="clearfix m_bottom_10">
								<i class="fa fa-phone f_left color_dark"></i>
								<p class="contact_e">(443) 340 4537</p>
							</div>
						</li>
						<li class="m_bottom_10">
							<div class="clearfix m_bottom_10">
								<i class="fa fa-envelope f_left color_dark"></i>
								<a class="contact_e default_t_color" href="mailto:#">contacto@hondaaltozano.com</a>
							</div>
						</li>
						<li>
							<div class="clearfix">
								<i class="fa fa-clock-o f_left color_dark"></i>
								<p class="contact_e">Lunes - Viernes: 09.00-19.00 <br>Sabado: 09.00-13.00<br> Domingo: cerrado</p>
							</div>
						</li>
					</ul>
				</div>	
			</li>
		</ul>
		 
		<!--login popup-->
		<div class="popup_wrap d_none" id="login_popup">
			<section class="popup r_corners shadow">
				<button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
				<h3 class="m_bottom_20 color_dark">Promocion X</h3>
				<form>
					<ul>
						<li class="m_bottom_15">
							<label for="username" class="m_bottom_5 d_inline_b">Nombre</label><br>
							<input type="text" name="" id="username" class="r_corners full_width">
						</li>
						<li class="m_bottom_15">
							<label for="username" class="m_bottom_5 d_inline_b">Telefono</label><br>
							<input type="text" name="" id="username" class="r_corners full_width">
						</li>
						<li class="m_bottom_15">
							<label for="username" class="m_bottom_5 d_inline_b">Correo</label><br>
							<input type="text" name="" id="username" class="r_corners full_width">
						</li>
						<li class="clearfix m_bottom_30">
							<button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">Enviar</button>
						</li>
					</ul>
				</form>
				<footer class="bg_light_color_1 t_mxs_align_c">
					<h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">Donde estamos?</h3>
					<a href="#" role="button" class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Ubicacion</a>
				</footer>
			</section>
		</div>
		
		<button class="t_align_c r_corners type_2 tr_all_hover animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i></button>
		<!--scripts include-->
		<script src="js/jquery-2.1.0.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/retina.js"></script>
		<script src="js/jquery.themepunch.plugins.min.js"></script>
		<script src="js/jquery.themepunch.revolution.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/jquery.isotope.min.js"></script>
		<script src="js/jquery.tweet.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/jquery.custom-scrollbar.js"></script>
		<script src="js/scripts.js"></script>
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5306f8f674bfda4c"></script>
	</body>
</html>