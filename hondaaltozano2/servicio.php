<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
	<head>
		<title>Honda Altozano</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!--meta info-->
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<link rel="icon" type="image/ico" href="images/fav.ico">
		<!--stylesheet include-->
		<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/settings.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/owl.carousel.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/owl.transitions.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/jquery.custom-scrollbar.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/style.css">
		<!--font include-->
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<script src="js/modernizr.js"></script>
	</head>
	<body>
		<!--boxed layout-->
		<div class="wide_layout relative w_xs_auto">
			<!--[if (lt IE 9) | IE 9]>
				<div style="background:#fff;padding:8px 0 10px;">
				<div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i class="fa fa-exclamation-triangle scheme_color f_left m_right_10" style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:16%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank" style="margin-bottom:2px;">Update Now!</a></div></div></div></div>
			<![endif]-->
			<!--markup header-->
			<header role="banner" class="type_5">
				<!--header top part-->
				<section class="h_top_part">
					<div class="container">
						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-5 t_xs_align_c">
								<ul class="d_inline_b horizontal_list clearfix f_size_small users_nav">
									<li><a href="#" class="default_t_color">Bienvenido</a></li>
									<li><a href="#" class="default_t_color" data-popup="#login_popup">Promocion</a></li>
									<li><a href="#" class="default_t_color" data-popup="#login_popup">Aviso Pago</a></li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-2 t_align_c t_xs_align_c">
								<p class="f_size_small">Llama a la Agencia: <b class="color_dark"> <i class="fa fa-phone"></i> (443) 340-4537</b></p>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-10 t_align_r t_xs_align_c">
								<ul class="horizontal_list clearfix d_inline_b t_align_l f_size_small site_settings type_2">
									<li class="container3d relative">
										<a role="button" href="#" class="color_dark" id="lang_button"><img class="d_inline_middle m_right_10" src="images/mini_honda_altozano.png" alt="">Honda Altozano</a>
										<ul class="dropdown_list type_2 top_arrow color_light">
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
										</ul>
										
									</li>
									<li class="m_left_20 relative container3d">
										<a role="button" href="#" class="color_dark" id="currency_button"><img class="d_inline_middle m_right_10" src="images/mini_grupo_fame.png" alt="">Grupo FAME</a>
										<ul class="dropdown_list type_2 top_arrow color_light">
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
											<li><a href="#" class="tr_delay_hover color_light"><i class="fa fa-facebook"> Facebook</i></a></li>
										</ul>
										
									</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<!--header bottom part-->
				<section class="h_bot_part container">
					<div class="clearfix row">
						<div class="col-lg-6 col-md-6 col-sm-4 t_xs_align_c">
							<a href="index.html" class="logo m_xs_bottom_15 d_xs_inline_b">
								<img src="images/logo.png" alt="">
							</a>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-8 t_align_r t_xs_align_c">
							<ul class="d_inline_b horizontal_list clearfix t_align_l site_settings">
								<!--like-->
								<li>
									<a role="button" href="#" class="button_type_1 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none"><i class="fa fa-heart-o f_size_ex_large"></i><span class="count circle t_align_c">12</span></a>
								</li>
								<li class="m_left_5">
									<a role="button" href="#" class="button_type_1 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none"><i class="fa fa-files-o f_size_ex_large"></i><span class="count circle t_align_c">3</span></a>
								</li>
								<!--language settings-->
								<li class="m_left_5 relative container3d">
									<a role="button" href="#" class="button_type_2 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none" id="lang_button"><img class="d_inline_middle m_right_10 m_mxs_right_0" src="images/flag_en.jpg" alt=""><span class="d_mxs_none">English</span></a>
									<ul class="dropdown_list top_arrow color_light">
										<li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="images/flag_en.jpg" alt="">English</a></li>
										<li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="images/flag_fr.jpg" alt="">French</a></li>
										<li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="images/flag_g.jpg" alt="">German</a></li>
										<li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="images/flag_i.jpg" alt="">Italian</a></li>
										<li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="images/flag_s.jpg" alt="">Spanish</a></li>
									</ul>
								</li>
								<!--currency settings-->
								<li class="m_left_5 relative container3d">
									<a role="button" href="#" class="button_type_2 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none" id="currency_button"><span class="scheme_color">$</span> <span class="d_mxs_none">US Dollar</span></a>
									<ul class="dropdown_list top_arrow color_light">
										<li><a href="#" class="tr_delay_hover color_light"><span class="scheme_color">$</span> US Dollar</a></li>
										<li><a href="#" class="tr_delay_hover color_light"><span class="scheme_color">&#8364;</span> Euro</a></li>
										<li><a href="#" class="tr_delay_hover color_light"><span class="scheme_color">&#163;</span> Pound</a></li>
									</ul>
								</li>
								<!--shopping cart-->
								<li class="m_left_5 relative container3d" id="shopping_button">
									<a role="button" href="#" class="button_type_3 color_light bg_scheme_color d_block r_corners tr_delay_hover box_s_none">
										<span class="d_inline_middle shop_icon m_mxs_right_0">
											<i class="fa fa-shopping-cart"></i>
											<span class="count tr_delay_hover type_2 circle t_align_c">3</span>
										</span>
										<b class="d_mxs_none">$355</b>
									</a>
									<div class="shopping_cart top_arrow tr_all_hover r_corners">
										<div class="f_size_medium sc_header">Recently added item(s)</div>
										<ul class="products_list">
											<li>
												<div class="clearfix">
													<!--product image-->
													<img class="f_left m_right_10" src="images/shopping_c_img_1.jpg" alt="">
													<!--product description-->
													<div class="f_left product_description">
														<a href="#" class="color_dark m_bottom_5 d_block">Cursus eleifend elit aenean auctor wisi et urna</a>
														<span class="f_size_medium">Product Code PS34</span>
													</div>
													<!--product price-->
													<div class="f_left f_size_medium">
														<div class="clearfix">
															1 x <b class="color_dark">$99.00</b>
														</div>
														<button class="close_product color_dark tr_hover"><i class="fa fa-times"></i></button>
													</div>
												</div>
											</li>
											<li>
												<div class="clearfix">
													<!--product image-->
													<img class="f_left m_right_10" src="images/shopping_c_img_2.jpg" alt="">
													<!--product description-->
													<div class="f_left product_description">
														<a href="#" class="color_dark m_bottom_5 d_block">Cursus eleifend elit aenean auctor wisi et urna</a>
														<span class="f_size_medium">Product Code PS34</span>
													</div>
													<!--product price-->
													<div class="f_left f_size_medium">
														<div class="clearfix">
															1 x <b class="color_dark">$99.00</b>
														</div>
														<button class="close_product color_dark tr_hover"><i class="fa fa-times"></i></button>
													</div>
												</div>
											</li>
											<li>
												<div class="clearfix">
													<!--product image-->
													<img class="f_left m_right_10" src="images/shopping_c_img_3.jpg" alt="">
													<!--product description-->
													<div class="f_left product_description">
														<a href="#" class="color_dark m_bottom_5 d_block">Cursus eleifend elit aenean auctor wisi et urna</a>
														<span class="f_size_medium">Product Code PS34</span>
													</div>
													<!--product price-->
													<div class="f_left f_size_medium">
														<div class="clearfix">
															1 x <b class="color_dark">$99.00</b>
														</div>
														<button class="close_product color_dark tr_hover"><i class="fa fa-times"></i></button>
													</div>
												</div>
											</li>
										</ul>
										<!--total price-->
										<ul class="total_price bg_light_color_1 t_align_r color_dark">
											<li class="m_bottom_10">Tax: <span class="f_size_large sc_price t_align_l d_inline_b m_left_15">$0.00</span></li>
											<li class="m_bottom_10">Discount: <span class="f_size_large sc_price t_align_l d_inline_b m_left_15">$37.00</span></li>
											<li>Total: <b class="f_size_large bold scheme_color sc_price t_align_l d_inline_b m_left_15">$999.00</b></li>
										</ul>
										<div class="sc_footer t_align_c">
											<a href="#" role="button" class="button_type_4 d_inline_middle bg_light_color_2 r_corners color_dark t_align_c tr_all_hover m_mxs_bottom_5">View Cart</a>
											<a href="#" role="button" class="button_type_4 bg_scheme_color d_inline_middle r_corners tr_all_hover color_light">Checkout</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</section>

				<!--main menu container-->
				<section class="menu_wrap relative">
					<div class="container clearfix">
						<!--button for responsive menu-->
						<button id="menu_button" class="r_corners centered_db d_none tr_all_hover d_xs_block m_bottom_10">
							<span class="centered_db r_corners"></span>
							<span class="centered_db r_corners"></span>
							<span class="centered_db r_corners"></span>
						</button>
						<!--main menu-->
						<nav role="navigation" class="f_left f_xs_none d_xs_none">	
							<ul class="horizontal_list main_menu clearfix">
								<li class="relative f_xs_none m_xs_bottom_5"><a href="index.html" class="tr_delay_hover color_light tt_uppercase"><b>Home</b></a>
									<!--sub menu-->
									<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
										<ul class="sub_menu">
											<li><a class="color_dark tr_delay_hover" href="index.html">Home Variant 1</a></li>
											<li><a class="color_dark tr_delay_hover" href="index_layout_2.html">Home Variant 2</a></li>
											<li><a class="color_dark tr_delay_hover" href="index_layout_wide.html">Home Variant 3</a></li>
											<li><a class="color_dark tr_delay_hover" href="index_corporate.html">Home Variant 4</a></li>
										</ul>
									</div>
								</li>
								<li class="relative f_xs_none m_xs_bottom_5"><a href="index_layout_wide.html" class="tr_delay_hover color_light tt_uppercase"><b>Sliders</b></a>
									<!--sub menu-->
									<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
										<ul class="sub_menu">
											<li><a class="color_dark tr_delay_hover" href="index_layout_wide.html">Revolution Slider</a></li>
											<li><a class="color_dark tr_delay_hover" href="index.html">Camera Slider</a></li>
											<li><a class="color_dark tr_delay_hover" href="index_layout_2.html">Flex Slider</a></li>
										</ul>
									</div>
								</li>
								<li class="relative f_xs_none m_xs_bottom_5"><a href="category_grid.html" class="tr_delay_hover color_light tt_uppercase"><b>Shop</b></a>
									<!--sub menu-->
									<div class="sub_menu_wrap top_arrow d_xs_none tr_all_hover clearfix r_corners w_xs_auto">
										<div class="f_left f_xs_none">
											<b class="color_dark m_left_20 m_bottom_5 m_top_5 d_inline_b">Dresses</b>
											<ul class="sub_menu first">
												<li><a class="color_dark tr_delay_hover" href="#">Evening Dresses</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Casual Dresses</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Party Dresses</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Maxi Dresses</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Midi Dresses</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Strapless Dresses</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Day Dresses</a></li>
											</ul>
										</div>
										<div class="f_left m_left_10 m_xs_left_0 f_xs_none">
											<b class="color_dark m_left_20 m_bottom_5 m_top_5 d_inline_b">Accessories</b>
											<ul class="sub_menu">
												<li><a class="color_dark tr_delay_hover" href="#">Bags and Purces</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Belts</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Scarves</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Gloves</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Jewellery</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Sunglasses</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Hair Accessories</a></li>
											</ul>
										</div>
										<div class="f_left m_left_10 m_xs_left_0 f_xs_none">
											<b class="color_dark m_left_20 m_bottom_5 m_top_5 d_inline_b">Tops</b>
											<ul class="sub_menu">
												<li><a class="color_dark tr_delay_hover" href="#">Evening Tops</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Long Sleeved</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Short Sleeved</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Sleeveless</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Tanks</a></li>
												<li><a class="color_dark tr_delay_hover" href="#">Tunics</a></li>
											</ul>
										</div>
										<img src="images/woman_image_1.jpg" class="d_sm_none f_right m_bottom_10" alt="">
									</div>
								</li>
								<li class="relative f_xs_none m_xs_bottom_5"><a href="#" class="tr_delay_hover color_light tt_uppercase"><b>Portfolio</b></a>
									<!--sub menu-->
									<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
										<ul class="sub_menu">
											<li><a class="color_dark tr_delay_hover" href="portfolio_two_columns.html">Portfolio 2 Columns</a></li>
											<li><a class="color_dark tr_delay_hover" href="portfolio_three_columns.html">Portfolio 3 Columns</a></li>
											<li><a class="color_dark tr_delay_hover" href="portfolio_four_columns.html">Portfolio 4 Columns</a></li>
											<li><a class="color_dark tr_delay_hover" href="portfolio_masonry.html">Masonry Portfolio</a></li>
											<li><a class="color_dark tr_delay_hover" href="portfolio_single_1.html">Single Portfolio Post v1</a></li>
											<li><a class="color_dark tr_delay_hover" href="portfolio_single_2.html">Single Portfolio Post v2</a></li>
										</ul>
									</div>
								</li>
								<li class="relative f_xs_none m_xs_bottom_5"><a href="category_grid.html" class="tr_delay_hover color_light tt_uppercase"><b>Pages</b></a>
									<!--sub menu-->
									<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
										<ul class="sub_menu">
											<li><a class="color_dark tr_delay_hover" href="category_grid.html">Grid View Category Page</a></li>
											<li><a class="color_dark tr_delay_hover" href="category_list.html">List View Category Page</a></li>
											<li><a class="color_dark tr_delay_hover" href="category_no_products.html">Category Page Without Products</a></li>
											<li><a class="color_dark tr_delay_hover" href="product_page_sidebar.html">Product Page With Sidebar</a></li>
											<li><a class="color_dark tr_delay_hover" href="full_width_product_page.html">Full Width Product Page</a></li>
                                            <li><a class="color_dark tr_delay_hover" href="wishlist.html">Wishlist</a></li>
											<li><a class="color_dark tr_delay_hover" href="compare_products.html">Compare Products</a></li>
											<li><a class="color_dark tr_delay_hover" href="checkout.html">Checkout</a></li>
											<li><a class="color_dark tr_delay_hover" href="manufacturers.html">Manufacturers</a></li>
											<li><a class="color_dark tr_delay_hover" href="manufacturer_details.html">Manufacturer Page</a></li>
											<li><a class="color_dark tr_delay_hover" href="orders_list.html">Orders List</a></li>
											<li><a class="color_dark tr_delay_hover" href="order_details.html">Order Details</a></li>
										</ul>
									</div>
								</li>
								<li class="relative f_xs_none m_xs_bottom_5"><a href="blog.html" class="tr_delay_hover color_light tt_uppercase"><b>Blog</b></a>
									<!--sub menu-->
									<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
										<ul class="sub_menu">
											<li><a class="color_dark tr_delay_hover" href="blog.html">Blog page</a></li>
											<li><a class="color_dark tr_delay_hover" href="blog_post.html">Single Blog Post page</a></li>
										</ul>
									</div>
								</li>
								<li class="current relative f_xs_none m_xs_bottom_5"><a href="features_shortcodes.html" class="tr_delay_hover color_light tt_uppercase"><b>Features</b></a>
									<!--sub menu-->
									<div class="sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners">
										<ul class="sub_menu">
											<li><a class="color_dark tr_delay_hover" href="features_shortcodes.html">Shortcodes</a></li>
											<li><a class="color_dark tr_delay_hover" href="features_typography.html">Typography</a></li>
										</ul>
									</div>
								</li>
								<li class="relative f_xs_none m_xs_bottom_5"><a href="contact.html" class="tr_delay_hover color_light tt_uppercase"><b>Contact</b></a></li>
							</ul>
						</nav>
						<button class="f_right search_button tr_all_hover f_xs_none d_xs_none">
							<i class="fa fa-search"></i>
						</button>
					</div>
					<!--search form-->
					<div class="searchform_wrap tf_xs_none tr_all_hover">
						<div class="container vc_child h_inherit relative">
							<form role="search" class="d_inline_middle full_width">
								<input type="text" name="search" placeholder="Type text and hit enter" class="f_size_large">
							</form>
							<button class="close_search_form tr_all_hover d_xs_none color_dark">
								<i class="fa fa-times"></i>
							</button>
						</div>
					</div>
				</section>


			</header>
			<!--slider-->
			<section class="revolution_slider">
				<div class="r_slider">
					<ul>
						 
						 
						<li class="f_left" data-transition="cube" data-slotamount="7" data-custom-thumb="images/banner_2.jpg">
							<img src="images/banner_2.jpg" alt="" data-bgrepeat="no-repeat" data-bgfit="cover" data-bgposition="center center">
							 <div class="caption sfb stb color_light slider_title tt_uppercase t_align_c" data-x="center" data-y="246" data-speed="1000" data-easing="ease" data-start="2500"><b class="color_white">Bienvenido a la Web Oficial de Honda Altozano</b></div>
							 
						</li>

						<li class="f_left" data-transition="cube" data-slotamount="7" data-custom-thumb="images/banner_1.jpg">
							<img src="images/banner_1.jpg" alt="" data-bgrepeat="no-repeat" data-bgfit="cover" data-bgposition="center center">
							<div class="caption lft ltt" data-x="center" data-y="58" data-speed="1500" data-start="1200" data-easing="easeOutBounce">
								<img src="images/slider_layer_img.png" alt="">
							</div>
							<div class="caption sfb stb color_light slider_title tt_uppercase t_align_c" data-x="center" data-y="246" data-speed="1000" data-easing="ease" data-start="2500"><b class="color_white">Promocion</b></div>
							<div class="caption sfb stb color_light" data-x="center" data-y="352" data-speed="1000" data-start="2900">
								<a href="#" role="button" class="button_type_4 bg_scheme_color color_light r_corners tt_uppercase">Ver Mas</a>
							</div>
						</li>

					</ul>
				</div>
			</section>
			<!--content-->
			<div class="page_content_offset">
				<div class="container">
					<!--banners-->
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
							<figure class="info_block_type_2 t_align_c">
								<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-thumbs-o-up d_inline_middle"></i></div>
								<h4 class="fw_medium color_dark m_bottom_15">COTIZA TU AUTO</h4>
								 
								 
							</figure>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
							<figure class="info_block_type_2 t_align_c">
								<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-expand d_inline_middle"></i></div>
								<h4 class="fw_medium color_dark m_bottom_15">CITA DE MANEJO</h4>
								 
								 
							</figure>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
							<figure class="info_block_type_2 t_align_c">
								<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-cogs d_inline_middle"></i></div>
								<h4 class="fw_medium color_dark m_bottom_15">CITA DE SERVICIO</h4>
								 
								 
							</figure>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 m_bottom_45 m_xs_bottom_30">
							<figure class="info_block_type_2 t_align_c">
								<div class="icon_wrap_1 r_corners bg_light_color_1 m_bottom_15 t_align_c vc_child scheme_color d_inline_b tr_all_hover"><i class="fa fa-wrench d_inline_middle"></i></div>
								<h4 class="fw_medium color_dark m_bottom_15">REFACCIONES</h4>
								 
								 
							</figure>
						</div>
					</div>
					
						 
					
					<!--product brands-->
					<div class="clearfix m_bottom_25 m_sm_bottom_20">
						<h2 class="tt_uppercase color_dark f_left heading2 animate_fade f_mxs_none m_mxs_bottom_15">Nuestros Vehiculos</h2>
						<div class="f_right clearfix nav_buttons_wrap animate_fade f_mxs_none">
							<button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left tr_delay_hover r_corners pb_prev"><i class="fa fa-angle-left"></i></button>
							<button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners pb_next"><i class="fa fa-angle-right"></i></button>
						</div>
					</div>
					<!--product brands carousel-->
					<div class="product_brands m_sm_bottom_35 m_xs_bottom_0">
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a>
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a> 
						<a href="#" class="d_block t_align_c animate_fade"><img src="images/autos/fit2019.png" alt=""><h4 class="fw_medium m_bottom_15"><b class="f_size_ex_large">FIT 2019</b></h2></a>  
					</div>
				</div>
			</div>

			<section class="bg_scheme_color call_to_action_2 m_bottom_45">
					<div class="container">
						<div class="d_table full_width cta_1 d_xs_block">
							<div class="d_table_cell v_align_m d_xs_block m_xs_bottom_30">
								<h1 class="color_light"><b class="f_size_ex_large">Contacta con Nosotros</b></h1>
							</div>
							<div class="d_table_cell v_align_m t_align_r d_xs_block t_xs_align_l">
								<button class="tr_delay_hover r_corners button_type_12 bg_light_color_2 color_dark f_size_large">Contactar!</button>
							</div>
						</div>
					</div>
				</section>


			<!--markup footer-->
			<footer id="footer">
				 
					 
					 
					<div class="container">
						<div class="row clearfix">
							<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_20 m_xs_bottom_10">
								<!--social icons-->
								<ul class="clearfix horizontal_list social_icons">
									<li class="facebook m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Facebook</span>
										<a href="#" class="r_corners t_align_c tr_delay_hover f_size_ex_large">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
									<li class="twitter m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Twitter</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
									<li class="google_plus m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Google Plus</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-google-plus"></i>
										</a>
									</li>
									<li class="rss m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Rss</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-rss"></i>
										</a>
									</li>
									<li class="pinterest m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Pinterest</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-pinterest"></i>
										</a>
									</li>
									<li class="instagram m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Instagram</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-instagram"></i>
										</a>
									</li>
									<li class="linkedin m_bottom_5 m_left_5 m_sm_left_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">LinkedIn</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-linkedin"></i>
										</a>
									</li>
									<li class="vimeo m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Vimeo</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-vimeo-square"></i>
										</a>
									</li>
									<li class="youtube m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Youtube</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-youtube-play"></i>
										</a>
									</li>
									<li class="flickr m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Flickr</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-flickr"></i>
										</a>
									</li>
									<li class="envelope m_left_5 m_bottom_5 relative">
										<span class="tooltip tr_all_hover r_corners color_dark f_size_small">Contact Us</span>
										<a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
											<i class="fa fa-envelope-o"></i>
										</a>
									</li>
								</ul>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_20 m_xs_bottom_30">
								<h3 class="color_light_2 d_inline_middle m_md_bottom_15 d_xs_block">Notifame</h3>
								<form id="newsletter" class="d_inline_middle m_left_5 subscribe_form_2 m_md_left_0">
									<input type="email" placeholder="Tu correo electronico" class="r_corners f_size_medium w_sm_auto m_mxs_bottom_15" name="newsletter-email">
									<button type="submit" class="button_type_8 r_corners bg_scheme_color color_light tr_all_hover m_left_5 m_mxs_left_0">Suscribirse</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!--copyright part-->

<?php date_default_timezone_set('America/mexico_city'); 
$lugar="home";
$anio=date('Y'); ?>


				<div class="footer_bottom_part">
					<div class="container clearfix t_mxs_align_c">
						<p class="f_left f_mxs_none m_mxs_bottom_10">&reg; <?php echo $anio; ?> <span class="color_light">Grupo FAME - Honda Altozano</span>. <i class="fa fa-user"> </i>  <a href="aviso.php" target="_self">Aviso de privacidad</a> <i class="fa fa-phone"></i> 01800 670 8386 </p>
						<ul class="f_right horizontal_list clearfix f_mxs_none d_mxs_inline_b">
							<li><img src="images/payment_img_1.png" alt=""></li>
							<li class="m_left_5"><img src="images/payment_img_2.png" alt=""></li>
							<li class="m_left_5"><img src="images/payment_img_3.png" alt=""></li>
							<li class="m_left_5"><img src="images/payment_img_4.png" alt=""></li>
							<li class="m_left_5"><img src="images/payment_img_5.png" alt=""></li>
						</ul>
					</div>
				</div>
			</footer>
		</div>
		<!--social widgets-->
		<ul class="social_widgets d_xs_none">
			<!--facebook-->
			<li class="relative">
				<button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Siguenos en Facebook</h3>
					<iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266" style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
				</div>
			</li>

			<!--twitter feed-->
			<li class="relative">
				<button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Latest Tweets</h3>
					<div class="twitterfeed m_bottom_25"></div>
					<a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color" href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
				</div>	
			</li>

			<!--contact info-->
			<li class="relative">
				<button class="sw_button t_align_c googlemap"><i class="fa fa-youtube-play"></i></button>
				 	<div class="sw_content">
					<h3 class="color_dark m_bottom_20">YouTube</h3>
					 
					<script src="https://apis.google.com/js/platform.js"></script>

                   <div class="g-ytsubscribe" data-channel="GrupoFameAutos" data-layout="default" data-count="hidden"></div>
				</div>
			</li>

			<!--twitter feed-->
			<li class="relative">
				<button class="sw_button t_align_c twitter"><i class="fa fa-bold"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Tweets</h3>
					<div class="twitterfeed m_bottom_25"></div>
					<a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color" href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
				</div>	
			</li>

			<!--contact form-->
			<li class="relative">
				<button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Contactanos</h3>
					<p class="f_size_medium m_bottom_15">Dejanos tus datos y en breve uno de nustros asesores te atendera.</p>
					<form id="contactform" class="mini">
						<input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name" placeholder="Nombre">
						<input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email" placeholder="Correo">
						<textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Mensaje" name="cf_message"></textarea>
						<button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">Enviar</button>
					</form>
				</div>	
			</li>
			<!--contact info-->
			<li class="relative">
				<button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Honda Altozano</h3>
					<ul class="c_info_list">
						<li class="m_bottom_10">
							<div class="clearfix m_bottom_15">
								<i class="fa fa-map-marker f_left color_dark"></i>
								<p class="contact_e">Paseo Altozano Zona Automotriz<br> Av. Montaña Monarca #1000</p>
							</div>
							<iframe class="r_corners full_width" id="gmap_mini" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.88754430981!2d-101.1644910846813!3d19.67480253798855!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d120a88d7dc89%3A0x945ba1aec77d55cf!2sHonda+Altozano+FAME!5e0!3m2!1ses!2smx!4v1551998396768"></iframe>

						</li>
						<li class="m_bottom_10">
							<div class="clearfix m_bottom_10">
								<i class="fa fa-phone f_left color_dark"></i>
								<p class="contact_e">(443) 340 4537</p>
							</div>
						</li>
						<li class="m_bottom_10">
							<div class="clearfix m_bottom_10">
								<i class="fa fa-envelope f_left color_dark"></i>
								<a class="contact_e default_t_color" href="mailto:#">contacto@hondaaltozano.com</a>
							</div>
						</li>
						<li>
							<div class="clearfix">
								<i class="fa fa-clock-o f_left color_dark"></i>
								<p class="contact_e">Lunes - Viernes: 09.00-19.00 <br>Sabado: 09.00-13.00<br> Domingo: cerrado</p>
							</div>
						</li>
					</ul>
				</div>	
			</li>
		</ul>
		 
	 
		<button class="t_align_c r_corners type_2 tr_all_hover animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i></button>
		<!--scripts include-->
		<script src="js/jquery-2.1.0.min.js"></script>
		<script src="js/retina.js"></script>
		<script src="js/jquery.themepunch.plugins.min.js"></script>
		<script src="js/jquery.themepunch.revolution.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/jquery.tweet.min.js"></script>
		<script src="js/jquery.custom-scrollbar.js"></script>
		<script src="js/scripts.js"></script>
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5306f8f674bfda4c"></script>
	</body>
</html>