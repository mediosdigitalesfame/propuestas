<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Mantenimiento Cadillac</title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="assets/css/essentialsazul.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layoutazul.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout-shopazul.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/color/azul.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>

 	<body class="smoothscroll enable-animation">

		 


		<!-- wrapper -->
		<div id="wrapper">

			<!-- Top Bar -->
				<div id="topBar" class="dark">
					<div class="container">

						<!-- right -->
						<ul class="top-links list-inline pull-right">
							<li class="text-welcome hidden-xs">Bienvenido a , <strong>Grupo FAME</strong></li>
							<li>
								<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-cog hidden-xs"></i> OTROS</a>
								<ul class="dropdown-menu pull-right">
									<li><a tabindex="-1" href="#"><i class="fa fa-history"></i> 1</a></li>
									<li class="divider"></li>
									<li><a tabindex="-1" href="#"><i class="fa fa-bookmark"></i> 2</a></li>
									<li><a tabindex="-1" href="#"><i class="fa fa-edit"></i> 3</a></li>
									<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i> 4</a></li>
									<li class="divider"></li>
									<li><a tabindex="-1" href="#"><i class="glyphicon glyphicon-off"></i> 5</a></li>
								</ul>
							</li>
						</ul>

						<!-- left -->
						<ul class="top-links list-inline">
							<li class="hidden-xs"><a href="page-contact-1.html">AAA</a></li>
							<li>
								<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">BBB</a>
								 
							</li>
							<li>
								<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">CCC</a>
								 
							</li>
						</ul>

					</div>
				</div>
				<!-- /Top Bar -->

			 
			<div id="header" class="sticky dark shadow-after-3 clearfix">

					<!-- TOP NAV -->
					<header id="topNav">

						<div class="container">

							<!-- Mobile Menu Button -->
							<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
								<i class="fa fa-bars"></i>
							</button>

							<!-- BUTTONS -->
							<ul class="pull-right nav nav-pills nav-second-main">

								<!-- SEARCH -->
								<li class="search">
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
									
								</li>
								<!-- /SEARCH -->

							</ul>
							<!-- /BUTTONS -->


							<!-- Logo -->
							<a class="logo pull-left" href="index.html">
								<img src="assets/images/logoagencias/famemanantiales.png" alt="" />
							</a>

							 
							<div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
								<nav class="nav-main">
									 
									<ul id="topMain" class="nav nav-pills nav-main">
										<li class="dropdown active"><!-- HOME -->
											<a class="dropdown-toggle" href="#">
												INICIO
											</a>
											<ul class="dropdown-menu">
												<li class="dropdown">
													<a class="dropdown-toggle" href="#">
														1
													</a>
													<ul class="dropdown-menu">
														<li><a href="#"> 1</a></li>
														<li><a href="#"> 2</a></li>
														<li><a href="#"> 3</a></li>
														<li><a href="#"> 4</a></li>
														<li><a href="#"> 5</a></li>
														<li><a href="#"> 6</a></li>
														<li><a href="#"> 7</a></li>
													</ul>
												</li>
											</ul>
										</li>

										<li class="dropdown"><!-- PAGES -->
											<a class="dropdown" href="autos.php">
												VEHICULOS 
											</a>


											<ul class="dropdown-menu">
												<li class="dropdown">
													<a class="dropdown-toggle" href="autos.php">
														MODELO 1
													</a>
													<ul class="dropdown-menu">
														<li><a href="#">VERSION 1</a></li>
														<li><a href="#">VERSION 2</a></li>
														<li><a href="#">VERSION 3</a></li>

													</ul>
												</li>

												<li><a href="page-blank.html">MODELO 2</a></li>
											</ul>

										</li>

										<li class="dropdown"><!-- FEATURES -->
											<a class="dropdown-toggle" href="#">
												COMPRAR
											</a>
											<ul class="dropdown-menu">
												<li class="dropdown">
													<a class="dropdown-toggle" href="#">
														<i class="et-browser"></i> PROMOCIONES
													</a>
													<ul class="dropdown-menu">

														<li><a href="#">Promo 1</a></li>
														<li><a href="#">Promo 2</a></li>
														<li><a href="#">Promo 3</a></li>
														
													</ul>
												</li>
												<li><a href="#"><i class="et-expand"></i> COTIZADOR</a></li>
												<li><a href="financiamiento.php"><i class="et-grid"></i> FINANCIAMIENTO</a></li>
												<li><a href="#"><i class="et-heart"></i> PRUEBA DE MANEJO</a></li>											 
												<li><a target="_blank" href="#"><span class="label label-success pull-right">NUEVO</span><i class="et-gears"></i> NUEVO</a></li>
											</ul>
										</li>


										<li class="dropdown"><!-- BLOG -->
											<a class="dropdown-toggle" href="#">
												SEMINUEVOS
											</a>
											<ul class="dropdown-menu">
												<li class="dropdown">
													<a   href="#">
														VER CATALOGO
													</a>

												</li>
												<li class="dropdown">
													<a   href="#">
														TOMAR AUTO A CUENTA
													</a>

												</li>
											</ul>
										</li>
										<li class="dropdown"><!-- SHOP -->
											<a class="dropdown-toggle" href="#">
												SERVICIO
											</a>
											<ul class="dropdown-menu pull-right">
												<li class="dropdown">
													<a   href="citaservicio.php">
														AGENDAR CITA
													</a>

												</li>

												<li><a href="refacciones.php">COTIZAR REFACCIONES</a></li>
												<li><a href="mantenimiento.php">SERVICIO DE MANTENIMIENTO</a></li>
												<li><a href="shop-checkout.html">GARANTIA EXTENDIDA </a></li>
												<li><a href="shop-checkout-final.html">ASISTENCIA VIAL</a></li>
											</ul>
										</li>
										<li class="dropdown mega-menu"><!-- SHORTCODES -->
											<a href="#">
												CONTACTO
											</a>

										</li>
									</ul>

								</nav>
							</div>
						</div>
					</header>
					<!-- /Top Nav -->

				</div>



			 


			<!-- INFO BAR -->
			<section class="info-bar info-bar-dark">
				<div class="container">

					<div class="row">

						<div class="col-sm-12">
							 
							<h3>Mantenimiento Vehiculos Cadillac</h3>
							 
						</div>

					</div>

				</div>
			</section>
			<!-- /INFO BAR -->


			<!-- FEATURED -->
			<section>
				<div class="container">

					<div class="owl-carousel featured nomargin owl-padding-10" data-plugin-options='{"loop":true,"singleItem": true, "items": "2", "stopOnHover":true, "autoPlay":4000, "autoHeight": true, "navigation": true, "pagination": false}'>


						<!-- GMC  -->
				<div class="shop-item nomargin">

					<div class="thumbnail">
						<!-- product image(s) -->
						<a class="shop-item-image" href="mantenimientomorelia.php">
							 
							<img class="img-responsive" src="assets/images/marcas/gmc/mantenimientocadillac.jpg" alt="Sin imagen" />
						</a>

						<!-- /product image(s) -->

					</div>

					<div class="shop-item-summary text-center">
						 
						 
					</div>
					 
				</div>
				<!-- /GMC   -->

						 

				 

			 

				 
					</div>

				</div>
			</section>
			<!-- /FEATURED -->





			<!-- FOOTER -->
					<footer id="footer">
						<div class="container">

							<div class="row">

								<div class="col-md-3">
									<!-- Footer Logo -->
									<img class="footer-logo" src="assets/images/logo-footer.png" alt="" />

									<!-- Small Description -->
									<p>Nos encuentras ubicados en:</p>

									<!-- Contact Address -->
									<address>
										<ul class="list-unstyled">
											<li class="footer-sprite address">
												Calle 21132<br>
												Colonia<br>
												Ciudad Estado<br>
											</li>
											<li class="footer-sprite phone">
												Telefono: 1-800-565-2390
											</li>
											<li class="footer-sprite email">
												<a href="mailto:support@yourname.com">correo@fame.com</a>
											</li>
										</ul>
									</address>
									<!-- /Contact Address -->

								</div>

								<div class="col-md-3">

									<!-- facebook -->
									<div id="fb-root"></div>

								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2';
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-page" data-href="https://www.facebook.com/grupofame" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-width="263" data-height="258">
									<blockquote cite="https://www.facebook.com/grupofame" class="fb-xfbml-parse-ignore">
										<a href="https://www.facebook.com/grupofame">Facebook</a>
									</blockquote>
								</div>
									<!-- /facebook -->

								</div>

								<div class="col-md-2">

									<!-- Links -->
									<h4 class="letter-spacing-1">EXPLORAR</h4>
									<ul class="footer-links list-unstyled">
										<li><a href="#">Inicio</a></li>
										<li><a href="#">Nosotros</a></li>
										<li><a href="#">Servicios</a></li>
										<li><a href="#">Precios</a></li>
										<li><a href="#">Contactanos</a></li>
									</ul>
									<!-- /Links -->

								</div>

								<div class="col-md-4">

									 <?php require_once('assets/auth/load.php'); ?>

									<!-- Newsletter Form -->
									<h4 class="letter-spacing-1">Mantente Informado</h4>
									<p>Suscribete para recibir informacion y ofertas.</p>

									<form class="validate" action="php/newsletter.php" method="post" data-success="Gracias! por Suscribirte!" data-toastr-position="bottom-right">
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="email" id="email" name="email" class="form-control required" placeholder="Ingresa tu Email">
											<span class="input-group-btn">
												<button class="btn btn-success" type="submit">Suscribirme</button>
											</span>
										</div>
									</form>
									<!-- /Newsletter Form -->
								</div>
							</div>
						</div>

						<div class="copyright">
							<div class="container">
								<ul class="pull-right nomargin list-inline mobile-block">
									<li><a href="#">Terminos y Condiciones</a></li>
									<li>&bull;</li>
									<li><a href="#">Privacy</a></li>
								</ul>
								&reg; Todos los derechos reservados.
							</div>
						</div>
					</footer>
					<!-- /FOOTER -->



			 
			<div id="shopLoadModal" class="modal fade" data-autoload="true" data-autoload-delay="2000">
				<div class="modal-dialog modal-full">
					<div class="modal-content" style="background-image:url('assets/images/misc/shop/shop_modal.jpg');">

						<!-- header modal -->
						<div class="modal-header noborder">
							<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
						</div>

						<!-- body modal -->
						<div class="modal-body">

							<div class="block-content">

								<img src="assets/images/logo-footer-dark.png" alt="" />
								<p class="size-13 margin-bottom-20 margin-top-30">No olvides suscribirte a nuestra revista digital.</p>

								<!-- newsletter -->
								<div class="inline-search clearfix margin-bottom-20">
									<form action="php/newsletter.php" method="post" class="validate nomargin" data-success="Gracias por suscribirte" data-toastr-position="bottom-right" novalidate="novalidate">

										<input type="search" placeholder="Email" id="shop_email" name="shop_email" class="serch-input required">
										<button type="submit">
											<i class="fa fa-check"></i>
										</button>
									</form>
								</div>
								<!-- /newsletter -->

								<!-- Don't show this popup again -->
								<div class="size-11 text-left">
									<label class="checkbox pull-left">
										<input class="loadModalHide" type="checkbox" />
										<i></i> <span class="weight-300">No volver a mostrar esta ventana</span>
									</label>

								</div>
								<!-- /Don't show this popup again -->

							</div>

						</div>

					</div>
				</div>
			</div>
			<!-- /HOME SHOP - MODAL ON LOAD -->



		</div>
		<!-- /wrapper -->






		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>

		<script type="text/javascript" src="assets/js/scripts.js"></script>
		
		 
		<!-- REVOLUTION SLIDER -->
		<script type="text/javascript" src="assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/js/view/demo.revolution_slider.js"></script>

		<!-- PAGE LEVEL SCRIPTS -->
		<script type="text/javascript" src="assets/js/view/demo.shop.js"></script>
	</body>
</html>