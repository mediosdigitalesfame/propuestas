<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>Enclave 2019</title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="Grupo FAME" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="assets/css/essentialsazul.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layoutazul.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout-shopazul.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/color/azul.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>

	 
	<body class="smoothscroll enable-animation">

		 


		<!-- wrapper -->
		<div id="wrapper">

			 <!-- Top Bar -->
				<div id="topBar" class="dark">
					<div class="container">

						<!-- right -->
						<ul class="top-links list-inline pull-right">
							<li class="text-welcome hidden-xs">Bienvenido a , <strong>Grupo FAME</strong></li>
							<li>
								<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-cog hidden-xs"></i> OTROS</a>
								<ul class="dropdown-menu pull-right">
									<li><a tabindex="-1" href="#"><i class="fa fa-history"></i> 1</a></li>
									<li class="divider"></li>
									<li><a tabindex="-1" href="#"><i class="fa fa-bookmark"></i> 2</a></li>
									<li><a tabindex="-1" href="#"><i class="fa fa-edit"></i> 3</a></li>
									<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i> 4</a></li>
									<li class="divider"></li>
									<li><a tabindex="-1" href="#"><i class="glyphicon glyphicon-off"></i> 5</a></li>
								</ul>
							</li>
						</ul>

						<!-- left -->
						<ul class="top-links list-inline">
							<li class="hidden-xs"><a href="page-contact-1.html">AAA</a></li>
							<li>
								<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">BBB</a>
								 
							</li>
							<li>
								<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">CCC</a>
								 
							</li>
						</ul>

					</div>
				</div>
				<!-- /Top Bar -->


			<div id="header" class="sticky dark shadow-after-3 clearfix">

				<!-- TOP NAV -->
					<header id="topNav">

						<div class="container">

							<!-- Mobile Menu Button -->
							<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
								<i class="fa fa-bars"></i>
							</button>

							<!-- BUTTONS -->
							<ul class="pull-right nav nav-pills nav-second-main">

								<!-- SEARCH -->
								<li class="search">
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
									
								</li>
								<!-- /SEARCH -->

							</ul>
							<!-- /BUTTONS -->


							<!-- Logo -->
							<a class="logo pull-left" href="index.html">
								<img src="assets/images/logoagencias/famemanantiales.png" alt="" />
							</a>

							 
							<div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
								<nav class="nav-main">

								 
									<ul id="topMain" class="nav nav-pills nav-main">
										<li class="dropdown active"><!-- HOME -->
											<a class="dropdown-toggle" href="#">
												INICIO
											</a>
											<ul class="dropdown-menu">
												<li class="dropdown">
													<a class="dropdown-toggle" href="#">
														1
													</a>
													<ul class="dropdown-menu">
														<li><a href="#"> 1</a></li>
														<li><a href="#"> 2</a></li>
														<li><a href="#"> 3</a></li>
														<li><a href="#"> 4</a></li>
														<li><a href="#"> 5</a></li>
														<li><a href="#"> 6</a></li>
														<li><a href="#"> 7</a></li>
													</ul>
												</li>
											</ul>
										</li>

										<li class="dropdown"><!-- PAGES -->
											<a class="dropdown" href="autos.php">
												VEHICULOS 
											</a>


											<ul class="dropdown-menu">
												<li class="dropdown">
													<a class="dropdown-toggle" href="autos.php">
														MODELO 1
													</a>
													<ul class="dropdown-menu">
														<li><a href="#">VERSION 1</a></li>
														<li><a href="#">VERSION 2</a></li>
														<li><a href="#">VERSION 3</a></li>

													</ul>
												</li>

												<li><a href="page-blank.html">MODELO 2</a></li>
											</ul>

										</li>

										<li class="dropdown"><!-- FEATURES -->
											<a class="dropdown-toggle" href="#">
												COMPRAR
											</a>
											<ul class="dropdown-menu">
												<li class="dropdown">
													<a class="dropdown-toggle" href="#">
														<i class="et-browser"></i> PROMOCIONES
													</a>
													<ul class="dropdown-menu">

														<li><a href="#">Promo 1</a></li>
														<li><a href="#">Promo 2</a></li>
														<li><a href="#">Promo 3</a></li>
														
													</ul>
												</li>
												<li><a href="#"><i class="et-expand"></i> COTIZADOR</a></li>
												<li><a href="financiamiento.php"><i class="et-grid"></i> FINANCIAMIENTO</a></li>
												<li><a href="#"><i class="et-heart"></i> PRUEBA DE MANEJO</a></li>											 
												<li><a target="_blank" href="#"><span class="label label-success pull-right">NUEVO</span><i class="et-gears"></i> NUEVO</a></li>
											</ul>
										</li>


										<li class="dropdown"><!-- BLOG -->
											<a class="dropdown-toggle" href="#">
												SEMINUEVOS
											</a>
											<ul class="dropdown-menu">
												<li class="dropdown">
													<a   href="#">
														VER CATALOGO
													</a>

												</li>
												<li class="dropdown">
													<a   href="#">
														TOMAR AUTO A CUENTA
													</a>

												</li>
											</ul>
										</li>
										<li class="dropdown"><!-- SHOP -->
											<a class="dropdown-toggle" href="#">
												SERVICIO
											</a>
											<ul class="dropdown-menu pull-right">
												<li class="dropdown">
													<a   href="citaservicio.php">
														AGENDAR CITA
													</a>

												</li>

												<li><a href="refacciones.php">COTIZAR REFACCIONES</a></li>
												<li><a href="mantenimiento.php">SERVICIO DE MANTENIMIENTO</a></li>
												<li><a href="shop-checkout.html">GARANTIA EXTENDIDA </a></li>
												<li><a href="shop-checkout-final.html">ASISTENCIA VIAL</a></li>
											</ul>
										</li>
										<li class="dropdown mega-menu"><!-- SHORTCODES -->
											<a href="#">
												CONTACTO
											</a>

										</li>
									</ul>

								</nav>
							</div>
						</div>
					</header>
					<!-- /Top Nav -->

			</div>
 
 
			 

 
		 
			<!-- /PAGE HEADER -->



			<!-- -->
			<section>
				<div class="container">


					<!-- REFACCIONES -->
						<div class="col-md-12 col-sm-12">

							<h2>Buick Enclave 2019</h2>

							<p>

								<span ><strong> <h3>La SUV del futuro para las familias de hoy</h3></strong>  
								</p>

								<p>
									Con un diseño espectacular, tres filas de cómodos asientos (2-2-3) y flexibilidad de carga para llevar todo lo que necesitas, Buick Enclave 2019 es donde el tiempo familiar, se convierte en tiempo de calidad.
<img class="img-responsive" src="assets/images/marcas/gmc/enclaveinterior.jpg" alt="Sin imagen" />


<p>
Pantalla táctil de 8”, sistema OnStar®† 4G LTE y Full Mirror Display convierten a Buick Enclave en un vehículo moderno y conectado. Escucha tu música favorita, realiza llamadas con manos libres, elige la mejor ruta con los mapas integrados, todo mientras mantienes los ojos en el camino.
</p>

<img class="img-responsive" src="assets/images/marcas/gmc/enclavepantalla.jpg" alt="Sin imagen" />

<p>
Disfruta de un manejo suave al volante con Buick Enclave Avenir, equipada con AWD y Electronic Twin Clutch, que se activa automáticamente cuando las condiciones del camino lo exigen.*
</p>
<img align="center" class="img-responsive" src="assets/images/marcas/gmc/enclaveseguridad.jpg" alt="Sin imagen" />

<p>
El camino está lleno de sorpresas. Es por eso que Buick Enclave ofrece una amplia variedad de características inteligentes y avanzadas en tecnología para mantenerte seguro en todo momento.
</p>


<h4>¿Quieres conocer más?</h4>

<div class="row">
									 

									<div class="col-md-3">
										<a href="assets/pdfs/enclave2019.pdf" title=""></a><button type="submit" class="btn btn-primary">  COTIZAR</button>
									</div>

								</div>

 

*El equipamiento varía conforme a la versión. Consulta más información en piso de venta o descarga el catálogo aquí. Hipervínculo en la palabra <a href="assets/pdfs/enclave2019.pdf" title=""> aquí. </a>

								</p>

								<hr />


 



							</div>
							<!-- /REFACCIONES -->


					<div class="row">
									<div class="col-md-12">
										<button href="https://api.whatsapp.com/send?phone=524433250949&text=Hola,%20Quiero%20más%20información!" class="btn btn-success">
											<i class="fa fa-whatsapp fa-2x" target="_blank" title="WhatsApp" ></i> <font size="4"> WhatsApp</font> 
										</button>
									</div>
								</div>

								<br>
					
					<div class="row">

						<!-- FORM -->
						<div class="col-md-6 col-sm-6">

							<h3>Cotiza <strong><em>tu Enclave 2019</em></strong></h3>

							
 
							<!-- Alert Success -->
							<div id="alert_success" class="alert alert-success margin-bottom-30">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Thank You!</strong> Your message successfully sent!
							</div><!-- /Alert Success -->


							<!-- Alert Failed -->
							<div id="alert_failed" class="alert alert-danger margin-bottom-30">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>[SMTP] Error!</strong> Internal server error!
							</div><!-- /Alert Failed -->


							<!-- Alert Mandatory -->
							<div id="alert_mandatory" class="alert alert-danger margin-bottom-30">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Sorry!</strong> You need to complete all mandatory (*) fields!
							</div><!-- /Alert Mandatory -->


							<form action="php/contact.php" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="action" value="contact_send" />

									<div class="row">
										<div class="form-group">
											<div class="col-md-4">
												<label for="contact:name">Nombre *</label>
												<input required type="text" value="" class="form-control" name="contact[name][required]" id="contact:name">
											</div>
											<div class="col-md-4">
												<label for="contact:email">E-mail *</label>
												<input required type="email" value="" class="form-control" name="contact[email][required]" id="contact:email">
											</div>
											<div class="col-md-4">
												<label for="contact:phone">Telefono</label>
												<input type="text" value="" class="form-control" name="contact[phone]" id="contact:phone">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-8">
												<label for="contact:subject">Direccion *</label>
												<input required type="text" value="" class="form-control" name="contact[subject][required]" id="contact:subject">
											</div>
											<div class="col-md-4">
												<label for="contact_department">Modelo</label>
												<select class="form-control pointer" name="contact[department]">
													<option value="">--- Select ---</option>
													<option value="Marketing">Modelo 1</option>
													<option value="Webdesign">Modelo 2</option>
													<option value="Architecture">Modelo 3</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-md-12">
												<label for="contact:message">Mensaje *</label>
												<textarea required maxlength="10000" rows="8" class="form-control" name="contact[message]" id="contact:message"></textarea>
											</div>
										</div>
									</div>
									 

								</fieldset>

								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> ENVIAR MENSAJE</button>
									</div>
								</div>
							</form>

						</div>
						<!-- /FORM -->


						<!-- INFO -->
						<div class="col-md-3 col-sm-3">

							<h3>Información de Contacto</h3>

							 

							<p>
								<span class="block"><strong><i class="fa fa-map-marker"></i> Dirección:</strong> Periférico Paseo de la República #2655 Col. Las Camelinas  Morelia, Michoacán.</span>
								<span class="block"><strong><i class="fa fa-phone"></i> Tel:</strong> <a href="tel:(443) 334 4440">(443) 334 4440</a></span>
								<span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:mail@yourdomain.com">contacto@famemanantiales.com</a></span>
							</p>

							<hr />

							<p>
								<span class="block"><strong><i class="fa fa-phone"></i> Recepción:</strong> <a href="tel:(443) 334 4440">Ext. 0</a></span>
								<span class="block"><strong><i class="fa fa-phone"></i> Taller de Servicio:</strong> <a href="tel:(443) 334 4440">Ext. 100</a></span>
								<span class="block"><strong><i class="fa fa-phone"></i> Postventa:</strong> <a href="tel:(443) 334 4440">Ext. 101</a></span>
								<span class="block"><strong><i class="fa fa-phone"></i> Refacciones:</strong> <a href="tel:(443) 334 4440">Ext. 300</a></span>
								<span class="block"><strong><i class="fa fa-phone"></i> Ventas:</strong> <a href="tel:(443) 334 4440">Ext. 200</a></span>
								<span class="block"><strong><i class="fa fa-phone"></i> Seminuevos:</strong> <a href="tel:(443) 334 4440">Ext. 203</a></span>
								<span class="block"><strong><i class="fa fa-phone"></i> Financiamiento:</strong> <a href="tel:(443) 334 4440">Ext. 204</a></span>
								
							</p>

								<hr />

							<h3>WhatsApp</h3>

						

							<p>
								<span class="block"><strong><i class="fa fa-phone"></i> </strong> <a href="tel:4432279923">4432279923</a></span>
								 
								
							</p>

							




						</div>
						<!-- /INFO -->

						<!-- INFO -->
						<div class="col-md-3 col-sm-3">

							<h3>Horario de Atención</h3>

							<p>
								Con gusto esperamos tu llamada en nuestro Call Center, para cualquier duda, aclaración o sugerencia que quieras compartirnos en FAME Manantiales; te escuchamos y atendemos de manera personalizada.
							</p>

							<hr />
 

							<h4 class="font300">Ventas</h4>
							<p>
								<span class="block"><strong>Lunes - Viernes:</strong> 09:00 - 19:30 hrs.</span>
								<span class="block"><strong>Sábado:</strong> 09:00 - 17:00 hrs.</span>
								<span class="block"><strong>Domingo:</strong> 11:00 - 15:00 hrs.</span>
							</p>

							<h4 class="font300">Servicio y Administración</h4>
							<p>
								<span class="block"><strong>Lunes - Viernes:</strong> 09:00 - 19:30 hrs.</span>
								<span class="block"><strong>Sábado:</strong> 09:00 - 14:00 hrs.</span>
								<span class="block"><strong>Domingo:</strong> Cerrado</span>
							</p>


						</div>
						<!-- /INFO -->



						



					</div>

				</div>
			</section>
			<!-- / -->


<!-- FOOTER -->
					<footer id="footer">
						<div class="container">

							<div class="row">

								<div class="col-md-3">
									<!-- Footer Logo -->
									<img class="footer-logo" src="assets/images/logo-footer.png" alt="" />

									<!-- Small Description -->
									<p>Nos encuentras ubicados en:</p>

									<!-- Contact Address -->
									<address>
										<ul class="list-unstyled">
											<li class="footer-sprite address">
												Calle 21132<br>
												Colonia<br>
												Ciudad Estado<br>
											</li>
											<li class="footer-sprite phone">
												Telefono: 1-800-565-2390
											</li>
											<li class="footer-sprite email">
												<a href="mailto:support@yourname.com">correo@fame.com</a>
											</li>
										</ul>
									</address>
									<!-- /Contact Address -->

								</div>

								<div class="col-md-3">

									<!-- facebook -->
									<div id="fb-root"></div>

								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2';
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-page" data-href="https://www.facebook.com/grupofame" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-width="263" data-height="258">
									<blockquote cite="https://www.facebook.com/grupofame" class="fb-xfbml-parse-ignore">
										<a href="https://www.facebook.com/grupofame">Facebook</a>
									</blockquote>
								</div>
									<!-- /facebook -->

								</div>

								<div class="col-md-2">

									<!-- Links -->
									<h4 class="letter-spacing-1">EXPLORAR</h4>
									<ul class="footer-links list-unstyled">
										<li><a href="#">Inicio</a></li>
										<li><a href="#">Nosotros</a></li>
										<li><a href="#">Servicios</a></li>
										<li><a href="#">Precios</a></li>
										<li><a href="#">Contactanos</a></li>
									</ul>
									<!-- /Links -->

								</div>

								<div class="col-md-4">

									 <?php require_once('assets/auth/load.php'); ?>

									<!-- Newsletter Form -->
									<h4 class="letter-spacing-1">Mantente Informado</h4>
									<p>Suscribete para recibir informacion y ofertas.</p>

									<form class="validate" action="php/newsletter.php" method="post" data-success="Gracias! por Suscribirte!" data-toastr-position="bottom-right">
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="email" id="email" name="email" class="form-control required" placeholder="Ingresa tu Email">
											<span class="input-group-btn">
												<button class="btn btn-success" type="submit">Suscribirme</button>
											</span>
										</div>
									</form>
									<!-- /Newsletter Form -->
								</div>
							</div>
						</div>

						<div class="copyright">
							<div class="container">
								<ul class="pull-right nomargin list-inline mobile-block">
									<li><a href="#">Terminos y Condiciones</a></li>
									<li>&bull;</li>
									<li><a href="#">Privacy</a></li>
								</ul>
								&reg; Todos los derechos reservados.
							</div>
						</div>
					</footer>
					<!-- /FOOTER -->

		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>

		<script type="text/javascript" src="assets/js/scripts.js"></script>
		
		<!-- STYLESWITCHER - REMOVE -->
		<script async type="text/javascript" src="assets/plugins/styleswitcher/styleswitcher.js"></script>

		<!-- PAGELEVEL SCRIPTS -->
		<script type="text/javascript" src="assets/js/contact.js"></script>

		 

	</body>
</html>