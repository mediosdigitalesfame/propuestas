<?php include_once('load.php'); ?>
<?php
$req_fields = array('username','password' );
validate_fields($req_fields);
$username = remove_junk($_POST['username']);
$password = remove_junk($_POST['password']);

  if(empty($errors)){

    $user = authenticate_v2($username, $password);

        if($user):
           //create session with id
           $session->login($user['id']);
           //Update Sign in time
           updateLastLogIn($user['id']);
           // redirect user to group home page by user level
           if($user['user_level'] === '1'):
             $session->msg("s", "Hello ".$user['username'].", Bienvenido Administrador.");
             redirect('../../../admin/amd',false);
           elseif ($user['user_level'] === '2'):
              $session->msg("s", "Hello ".$user['username'].", Bienvenido Usuario.");
             redirect('special.php',false);
           else:
              $session->msg("s", "Hello ".$user['username'].", Bienvenido Invitado.");
             redirect('home.php',false);
           endif;

        else:
          $session->msg("d", "Usuario o Contraseña Incorrectos.");
          redirect('../../../admin/amd/login',false);
        endif;

  } else {

     $session->msg("d", $errors);
     redirect('login_v2.php',false);
  }

?>
