<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Unique Responsive Multipurpose Bootstrap 4 HTML Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contacto - Hona Altozan</title>

    <link rel="icon" type="image/ico" href="../assets/images/honda-altozano.ico">

    <!-- CSS files -->
    <link href="https://fonts.googleapis.com/css?family=Quattrocento:400,700%7COswald:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/animate.min.css">
    <link rel="stylesheet" href="../assets/css/corporate-classic.min.css">

    <!-- Fav Icons -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/favicon/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/favicon/favicon-32x32.png">

    
</head>

<body class="pace-on pace-squares">
    <div class="pace-overlay"></div>
    <!--[if lt IE 10]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <div class="verso-content-box verso-content-box-move-behind">

        <!-- Header -->

        <div class="verso-header verso-header-transparent">

            <!-- Top Bar -->
            <div class="verso-topbar verso-topbar-leftright">
                <div class="verso-topbar-inner">
                    <div class="verso-topbar-container">
                        <div class="verso-topbar-col">
                            <div class="verso-widget widget_text">
                                <i class="fa fa-phone"></i> &nbsp; 1-800-555-555
                            </div>
                        </div>
                        <div class="verso-topbar-col">
                            <div class="verso-widget widget_social">
                                <div class="verso-icon-set">
                                    <a class="verso-icon-set-item verso-transition" href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    <a class="verso-icon-set-item verso-transition" href="#">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <a class="verso-icon-set-item verso-transition" href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a class="verso-icon-set-item verso-transition" href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Navigation -->
            <div class="verso-nav verso-nav-sticky verso-nav-layout-logo-l-menu-r verso-nav-hover">
                <div class="verso-nav-inner">
                    <div class="verso-nav-container">

                        <!-- Logo -->
                        <div class="verso-nav-brand">
                            <a href="index.html">
                                <img src="../assets/images/corporate-classic-logo-130x100.png" alt="verso"> VERSO
                            </a>
                        </div>

                        <!-- Mobile menu toggle button -->
                        <div class="verso-nav-mobile">
                            <a id="nav-toggle" href="#">
                                <span></span>
                            </a>
                        </div>

                        <!-- Menu One -->
                        <nav class="verso-nav-menu">
                            <ul class="verso-nav-list">
                                <li>
                                    <a href="index.html">HOME</a>
                                </li>
                                <li>
                                    <a href="#">ABOUT</a>
                                    <ul class="verso-nav-dropdown closed">
                                        <li>
                                            <a href="about.html">ABOUT US</a>
                                        </li>
                                        <li>
                                            <a href="team.html">OUR TEAM</a>
                                        </li>
                                        <li>
                                            <a href="pricing.html">PRICING</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="services.html">SERVICES</a>
                                </li>
                                <li>
                                    <a href="#">PROJECTS</a>
                                    <ul class="verso-nav-dropdown closed">
                                        <li>
                                            <a href="projects.html">PROJECTS</a>
                                        </li>
                                        <li>
                                            <a href="project.html">SINGLE PROJECT</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">BLOG</a>
                                    <ul class="verso-nav-dropdown closed">
                                        <li>
                                            <a href="blog.html">BLOG</a>
                                        </li>
                                        <li>
                                            <a href="post.html">SINGLE POST</a>
                                        </li>
                                        <li>
                                            <a href="author.html">AUTHOR'S PAGE</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="contact.html">CONTACT</a>
                                </li>
                            </ul>
                            <div class="verso-nav-widget">
                                <div class="verso-widget">
                                    <div class="verso-icon-set">
                                        <a class="verso-icon-set-item verso-transition verso-search-widget-button-open" href="#">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>



        <!-- Content -->
        <div class="verso-content">

            <div class="section">
                <div class="section-background">
                    <div class="section-bg-image section-bg-image-size-cover section-bg-image-position-top section-bg-image-paralax verso-demo-bg-image-12"></div>
                </div>
                <div class="section-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <h1 class="display-3 text-uppercase verso-text-light verso-font-weight-300 verso-mt-25 verso-mb-15 verso-opacity-8 verso-os-animation text-lg-right" data-os-animation="fadeIn" data-os-animation-delay=".3s">
                                    <span class="text-muted">Contact </span> Us
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section">
                <div class="section-background" data-section-bg-title="CONTACT"></div>
                <div class="section-content">
                    <div class="container">
                        <div class="row verso-py-10 verso-mb-lg-10">
                            <div class="col-lg-3">
                                <h1 class="h2 verso-heading-image verso-heading-image-01 verso-heading-image-reversed text-uppercase  text-left text-lg-right verso-os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".3s">
                                    <small class="d-block text-muted">Contact</small> Details
                                </h1>
                            </div>
                            <div class="col-lg-9">
                                <div class="card card-hero card-horizontal verso-shadow-15 verso-shadow-hover-20 verso-transition verso-mb-3 verso-os-animation text-center verso-os-animation" data-os-animation="fadeIn" data-os-animation-delay=".4s">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card-body">
                                                <form action="contact_mailer.php" class="contact-form">
                                                    <input type="text" id="name" name="name" class="form-control verso-shadow-0 verso-shadow-focus-2 verso-transition verso-mb-3" placeholder="Name">
                                                    <input type="email" id="email" name="email" class="form-control verso-shadow-0 verso-shadow-focus-2 verso-transition verso-mb-3" placeholder="Your Email">
                                                    <textarea id="message" name="message" rows="8" class="form-control verso-shadow-0 verso-shadow-focus-2 verso-transition verso-mb-3" placeholder="Your Comment"></textarea>
                                                    <div class="form-check verso-mb-3">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" id="sign" name="sign" type="checkbox" value="sign"> Sign me up to the newsletter
                                                        </label>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary verso-shadow-2">Send</button>
                                                    <div class="verso-messages verso-pt-2"></div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6 verso-demo-bg-gradient-02 text-light">
                                            <div class="card-body text-left">
                                                <p>Completely synergize scalable e-commerce rather than high standards in e-services. Assertively iterate resource maximizing products after leading-edge intellectual capital.</p>
                                                <dl class="row verso-mb-0">
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">Street:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        28th Envato Street
                                                    </dd>
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">City:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        New York
                                                    </dd>
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">Phone No:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        555-7894-1144
                                                    </dd>
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">E-Mail:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        info@versotheme.org
                                                    </dd>
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">Manager:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        Gary Jones
                                                    </dd>
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">Facebook:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        Verso Theme Group
                                                    </dd>
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">Twitter:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        @Verso_Theme
                                                    </dd>
                                                    <dt class="col-md-4">
                                                        <span class="text-muted font-italic">Instagram:</span>
                                                    </dt>
                                                    <dd class="col-md-8">
                                                        One Theme to Rule them All.
                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section">
                <div class="section-background" data-section-bg-title="OFFICES">
                    <div class="section-bg-image section-bg-image-size-cover section-bg-image-position-top section-bg-image-paralax verso-demo-bg-image-02"></div>
                </div>
                <div class="section-content">
                    <div class="container">
                        <div class="row verso-pt-10 verso-pb-10 verso-text-light">
                            <div class="col-lg-3">
                                <h1 class="h2 verso-heading-image verso-heading-image-01 verso-heading-image-reversed text-uppercase  text-left text-lg-right verso-os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".3s">
                                    <small class="d-block text-muted">Our</small> Offices
                                </h1>
                            </div>
                            <div class="col-lg-9">
                                <div class="card card-hero card-inverse verso-shadow-15 verso-shadow-hover-20 verso-os-animation text-center verso-mt-lg--20 verso-photo-filter-grayscale" data-os-animation="fadeIn" data-os-animation-delay=".4s">
                                    <div class="google-map" id="map" style="height:450px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Footer -->

        <footer class="verso-footer">
            <div class="section verso-py-9">
                <div class="section-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="verso-widget widget_text">
                                    <h4 class="h5 verso-widget-header text-uppercase">About Us</h4>
                                    <p>Quickly coordinate e-business applications through revolutionary catalysts for change. Seamlessly underwhelm optimal testing procedures whereas bricks-and-clicks. </p>
                                    <p>Distinctively exploit optimal alignments for intuitive bandwidth. </p>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="verso-widget widget_categories">
                                    <h4 class="h5 verso-widget-header text-uppercase">Categories</h4>
                                    <ul>
                                        <li>
                                            <a href="blog.html">
                                                Human Resources (3)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="blog.html">
                                                Web Development (2)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="blog.html">
                                                Corporate Funding (1)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="blog.html">
                                                Marketing &amp; Media (3)
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="verso-widget widget_recent_entries">
                                    <h4 class="h5 verso-widget-header text-uppercase">Recent Posts</h4>

                                    <ul>
                                        <li class="text-truncate">
                                            <a href="post.html" class="float-left verso-mr-1 verso-widget-image" title="Post title">
                                                <img alt="post image" src="../assets/images/corporate-classic-article-11-100x100-notinclude.jpg">
                                            </a>
                                            <a href="post.html">Corporate Enviroment</a>
                                            <small class="post-date d-block">September 25, 2017</small>
                                        </li>

                                        <li class="text-truncate">
                                            <a href="post.html" class="float-left verso-mr-1 verso-widget-image" title="Post title">
                                                <img alt="post image" src="../assets/images/corporate-classic-article-12-100x100-notinclude.jpg">
                                            </a>
                                            <a href="post.html"> Better Sales</a>
                                            <small class="post-date d-block">June 12, 2017</small>
                                        </li>
                                        <li class="text-truncate">
                                            <a href="post.html" class="float-left verso-mr-1 verso-widget-image" title="Post title">
                                                <img alt="post image" src="../assets/images/corporate-classic-article-13-100x100-notinclude.jpg">
                                            </a>
                                            <a href="post.html">Stronger Leads & new visitors</a>
                                            <small class="post-date d-block">May 05, 2017</small>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="verso-widget widget_tag_cloud">
                                    <h4 class="h5 verso-widget-header text-uppercase">Tags</h4>
                                    <ul>
                                        <li>
                                            <a href="blog.html">company</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">leads</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">marketshare</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">events</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">resources</a>
                                        </li>
                                        <li>
                                            <a href="blog.html">capital</a>
                                        </li>
                                        <li>
                                            <a href="blog.html">enviroment</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">offices</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">offshore</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">accounting</a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section verso-py-1">
                <div class="section-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 text-sm-left">
                                <div class="verso-widget widget_text">
                                    <p>
                                        Verso Template. © Oxygenna 2017
                                    </p>
                                </div>

                            </div>
                            <div class="col-sm-6 text-sm-right">
                                <div class="verso-widget widget_social">
                                    <div class="verso-icon-set">
                                        <a class="verso-icon-set-item verso-transition" href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                        <a class="verso-icon-set-item verso-transition" href="#">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <a class="verso-icon-set-item verso-transition" href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a class="verso-icon-set-item verso-transition" href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </footer>

        <a href="#" class="verso-go-top verso-shadow-10 verso-shadow-hover-15 d-none d-sm-block hide">
            <i class="fa fa-angle-up"></i>
        </a>




    </div>

    <div class="verso-search-widget-container">
        <button class="verso-search-widget-button-close">
            <i class="fa fa-close"></i>
        </button>
        <form class="verso-search-widget-form" action="index.html">
            <input class="verso-search-widget-input" name="search" type="search" spellcheck="false" />
            <span class="verso-search-widget-info">Hit enter to search or ESC to close</span>
        </form>
    </div>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/magnific.min.js"></script>
    <script src="../assets/js/pace.min.js"></script>
    <script src="../assets/js/smooth-scroll.min.js"></script>
    <script src="../assets/js/waypoints.min.js"></script>
    <script src="../assets/js/prlx.min.js"></script>
    <script src="../assets/js/countdown.min.js"></script>
    <script src="../assets/js/corporate-classic.min.js"></script>


    <script type="text/javascript">
        var map = {
            map_type: 'ROADMAP',
            auto_zoom: 'manual', // set to 'manual' to set the zoom manually. Leave empty for auto zoom
            map_zoom: 7, // set the zoom if auto_zoom is set to
            map_scrollable: 'on', // makes the map draggable
            map_style: '', // if set to blackwhite it takes a black/white saturation
            addresses: ['London', 'Paris'], // addresses separated by comma. Addresses are picked up first
            latlngs: ['51.511084, -0.133202', '51.506623, -0.111916'], // Lat/Lng separated by comma. if no addresses are set these coordinates will get used
            labels: ['London', 'Paris'], // labels that will be added to the markeres respectively. Keep the same number as markers
            auto_center: 'auto', // center the map 'auto' or 'custom'
            center_latlng: '', // centers the map to this point, unless it is set to auto
            show_markers: 'on', // shows/Hides the markers
            markerURL: '../assets/images/marker.png' // the marker URL
        };
    </script>
    <script type="text/javascript" src="../assets/js/contact.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA53lVLoFr_BgRcgzx80ThvLm4vBJLp3x0"></script>

</body>

</html>