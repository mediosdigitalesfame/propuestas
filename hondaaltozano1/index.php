<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Honda Altozano</title>
	<meta name="keywords" content="HTML5,CSS3,Template" />
	<meta name="description" content="" />
	<meta name="Author" content="Grupo FAME" />

	<link rel="icon" type="image/ico" href="../assets/images/honda-altozano.ico">

	<!-- mobile settings -->
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

	<!-- WEB FONTS : use %7C instead of | (pipe) -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

	<!-- CORE CSS -->
	<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- LAYER SLIDER -->
	<link href="../assets/plugins/slider.layerslider/css/layerslider.css" rel="stylesheet" type="text/css" />

	<!-- REVOLUTION SLIDER -->
	<link href="../assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
	<link href="../assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

	<!-- THEME CSS -->
	<link href="../assets/css/essentialsrojo.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/layoutrojo.css" rel="stylesheet" type="text/css" />

	<!-- PAGE LEVEL SCRIPTS -->
	<link href="../assets/css/header-1.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/layout-shoprojo.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/color/rojo.css" rel="stylesheet" type="text/css" id="color_scheme" />

	<!-- SWIPER SLIDER -->
	<link href="../assets/plugins/slider.swiper/dist/css/swiper.min.css" rel="stylesheet" type="text/css" />

</head>

<body class="smoothscroll enable-animation">
	
	<!-- wrapper -->
	<div id="wrapper">

		<!-- Top Bar -->
		<div id="topBar" class="dark">
			<div class="container">

				<!-- right -->
				<ul class="top-links list-inline pull-right">
					<li class="text-welcome hidden-xs">Bienvenido a , <strong>Honda Altozano</strong></li>
					<li>
						<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-cog hidden-xs"></i>Nosotros</a>


						<ul class="dropdown-menu pull-right">
							<li><a tabindex="-1" href="#"><i class="fa fa-history"></i> Grupo FAME</a></li>
							<li class="divider"></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-bookmark"></i>Honda Monarca</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-edit"></i>Honda Manantiales</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i>Honda Atizapan</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i>Honda Monarca DF</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i>Honda Uruapan</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i>Honda Corregidora</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i>Honda Marquesa</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i>Honda San Juan del Río</a></li>
							<li><a tabindex="-1" href="#"><i class="fa fa-cog"></i>Honda 5 de Febrero</a></li>
							<li class="divider"></li>
							<li><a tabindex="-1" href="#"><i class="glyphicon glyphicon-off"></i>Seminuevos</a></li>
						</ul>
					</li>
				</ul>

				<!-- left -->
				<ul class="top-links list-inline">
					<li class="hidden-xs"><a href="page-contact-1.html"><i class="fa fa-phone"></i>Agencia: (443) 340 4537</a></li>
					<li>
						<a class="dropdown-toggle no-text-underline" href="images/pagarencaja.jpg" target="_blank" data-toggle="dropdown" href="#"><i class="fa fa-exclamation-circle"></i>Aviso pago </a>
						
					</li>

				</ul>

			</div>
		</div>
		<!-- /Top Bar -->
		
		<div id="header" class="sticky dark shadow-after-3 clearfix">

			<!-- TOP NAV -->
			<header id="topNav">
				<div class="container">

					<!-- Mobile Menu Button -->
					<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>

					<!-- BUTTONS -->
					<ul class="pull-right nav nav-pills nav-second-main">

						<!-- SEARCH -->
						<li class="search">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
							
						</li>

						<li class="search">
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
							
						</li>

						<li class="search">
							<a href="#">
								<i class="fa fa-youtube-play"></i>
							</a>
							
						</li>

						<li class="search">
							<a href="#">
								<i class="fa fa-instagram"></i>
							</a>
							
						</li>
						<li class="search">
							<a href="#">
								<i class="fa fa-whatsapp"></i>
							</a>
							
						</li>
						<!-- /SEARCH -->

					</ul>
					<!-- /BUTTONS -->


					<!-- Logo -->
					<a class="logo pull-left" href="index.html">
						<img src="../assets/images/hondaaltozano/logo_dark.png" alt="" />
					</a>
					
					<div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
						<nav class="nav-main">
							
							<ul id="topMain" class="nav nav-pills nav-main">
								<li class="dropdown active"><!-- HOME -->
									<a class="dropdown-toggle" href="#">
										INICIO
									</a>
									<ul class="dropdown-menu">
										<li class="dropdown">
											<a class="dropdown-toggle" href="#">
												1
											</a>
											<ul class="dropdown-menu">
												<li><a href="#"> 1</a></li>
												<li><a href="#"> 2</a></li>
												<li><a href="#"> 3</a></li>
												<li><a href="#"> 4</a></li>
												<li><a href="#"> 5</a></li>
												<li><a href="#"> 6</a></li>
												<li><a href="#"> 7</a></li>
											</ul>
										</li>
									</ul>
								</li>

								<li class="dropdown"><!-- PAGES -->
									<a class="dropdown-toggle" href="#">
										VEHICULOS
									</a>
									<ul class="dropdown-menu">
										<li class="dropdown">
											<a class="dropdown-toggle" href="#">
												MODELO 1
											</a>
											<ul class="dropdown-menu">
												<li><a href="#">VERSION 1</a></li>
												<li><a href="#">VERSION 2</a></li>
												<li><a href="#">VERSION 3</a></li>

											</ul>
										</li>
										<li><a href="page-blank.html">MODELO 2</a></li>
									</ul>
								</li>

								<li class="dropdown"><!-- FEATURES -->
									<a class="dropdown-toggle" href="#">
										COMPRAR
									</a>
									<ul class="dropdown-menu">
										<li class="dropdown">
											<a class="dropdown-toggle" href="#">
												<i class="et-browser"></i> PROMOCIONES
											</a>
											<ul class="dropdown-menu">

												<li><a href="#">Promo 1</a></li>
												<li><a href="#">Promo 2</a></li>
												<li><a href="#">Promo 3</a></li>
												
											</ul>
										</li>
										<li><a href="#"><i class="et-expand"></i> COTIZADOR</a></li>
										<li><a href="#"><i class="et-grid"></i> FINANCIAMIENTO</a></li>
										<li><a href="#"><i class="et-heart"></i> PRUEBA DE MANEJO</a></li>											 
										<li><a target="_blank" href="#"><span class="label label-success pull-right">NUEVO</span><i class="et-gears"></i> NUEVO</a></li>
									</ul>
								</li>


								<li class="dropdown"><!-- BLOG -->
									<a class="dropdown-toggle" href="#">
										SEMINUEVOS
									</a>
									<ul class="dropdown-menu">
										<li class="dropdown">
											<a   href="#">
												VER CATALOGO
											</a>

										</li>
										<li class="dropdown">
											<a   href="#">
												TOMAR AUTO A CUENTA
											</a>

										</li>
									</ul>
								</li>
								<li class="dropdown"><!-- SHOP -->
									<a class="dropdown-toggle" href="#">
										SERVICIO
									</a>
									<ul class="dropdown-menu pull-right">
										<li class="dropdown">
											<a   href="#">
												AGENDAR CITA
											</a>

										</li>

										<li><a href="shop-compare.html">COTIZAR REFACCIONES</a></li>
										<li><a href="shop-cart.html">SERVICIO DE MANTENIMIENTO</a></li>
										<li><a href="shop-checkout.html">GARANTIA EXTENDIDA </a></li>
										<li><a href="shop-checkout-final.html">ASISTENCIA VIAL</a></li>
									</ul>
								</li>
								<li class="dropdown mega-menu"><!-- SHORTCODES -->
									<a href="contacto.php">
										CONTACTO
									</a>

								</li>
							</ul>

						</nav>
					</div>

				</div>
			</header>
			<!-- /Top Nav -->

		</div>


		<!-- REVOLUTION SLIDER -->
		<div class="slider fullwidthbanner-container roundedcorners">
			
			<div class="fullwidthbanner" data-height="600" data-shadow="0" data-navigationStyle="preview2">
				<ul class="hide">

					<!-- SLIDE  -->
					<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Bienvenida" style="background-color: #F6F6F6;">

						<img src="../assets/images/backgrounds/grey-noise-min.jpg" alt="video" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

						<div class="tp-caption large_bold_grey skewfromrightshort customout font-open-sans"
						data-x="center"
						data-y="110"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="500"
						data-start="800"
						data-easing="Back.easeOut"
						data-endspeed="500"
						data-endeasing="Power4.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-weight:normal;">Bienvenido a la Web Oficial de  
					</div>

					<div class="tp-caption large_bold_grey skewfromrightshort customout font-open-sans"
					data-x="center"
					data-y="190"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="500"
					data-start="800"
					data-easing="Back.easeOut"
					data-endspeed="500"
					data-endeasing="Power4.easeIn"
					data-captionhidden="off"
					style="z-index: 4; font-weight:bold;"> Honda Altozano
				</div>

				<div class="tp-caption large_bold_grey skewfromrightshort customout font-open-sans"
				data-x="center"
				data-y="290"
				data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
				data-speed="500"
				data-start="800"
				data-easing="Back.easeOut"
				data-endspeed="500"
				data-endeasing="Power4.easeIn"
				data-captionhidden="off"
				style=" color: #333; font-size:35px; max-width: 550px; white-space: normal; text-shadow:none;">  
			</div>

			<div class="tp-caption large_bold_grey skewfromrightshort customout font-open-sans"
			data-x="center"
			data-y="340"
			data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
			data-speed="500"
			data-start="800"
			data-easing="Back.easeOut"
			data-endspeed="500"
			data-endeasing="Power4.easeIn"
			data-captionhidden="off"
			style=" color: #333; font-size:25px; white-space: normal; text-shadow:none;"> En nuestra página encontrarás vehículos nuevos , seminuevos ¡y mucho más! | <i class="fa fa-phone"></i> Tel:(443) 340 4537
		</div>
	</li>



	<!-- SLIDE -->
	<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Siguiente">

		<img src="../assets/images/1x1.png" data-lazyload="../assets/images/demo/video/back.jpg" alt="video" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

		<div class="tp-caption tp-fade fadeout fullscreenvideo"
		data-x="0"
		data-y="50"
		data-speed="1000"
		data-start="1100"
		data-easing="Power4.easeOut"
		data-elementdelay="0.01"
		data-endelementdelay="0.1"
		data-endspeed="1500"
		data-endeasing="Power4.easeIn"
		data-autoplay="true"
		data-autoplayonlyfirsttime="false"
		data-nextslideatend="true"
		data-volume="mute" 
		data-forceCover="1" 
		data-aspectratio="16:9" 
		data-forcerewind="on" style="z-index: 2;">

		<div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>

		<video class="" preload="none" style="widt:100%;height:100%" poster="../assets/images/demo/video/back.jpg">
			<source src="../assets/images/demo/video/back.webm" type="video/webm" />
			<source src="../assets/images/demo/video/back.mp4" type="video/mp4" />
		</video>

	</div>

	<div class="tp-caption customin ltl tp-resizeme text_white"
	data-x="center"
	data-y="155"
	data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
	data-speed="800"
	data-start="1000"
	data-easing="easeOutQuad"
	data-splitin="none"
	data-splitout="none"
	data-elementdelay="0.01"
	data-endelementdelay="0.1"
	data-endspeed="1000"
	data-endeasing="Power4.easeIn" style="z-index: 3;">
	<span class="weight-300">Texto Editable  / Texto Editable / Texto Editable  / Texto Editable </span>
</div>

<div class="tp-caption customin ltl tp-resizeme large_bold_white"
data-x="center"
data-y="205"
data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
data-speed="800"
data-start="1200"
data-easing="easeOutQuad"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.01"
data-endelementdelay="0.1"
data-endspeed="1000"
data-endeasing="Power4.easeIn" style="z-index: 3;">
Texto Editable 
</div>

<div class="tp-caption customin ltl tp-resizeme small_light_white font-lato"
data-x="center"
data-y="295"
data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
data-speed="800"
data-start="1400"
data-easing="easeOutQuad"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.01"
data-endelementdelay="0.1"
data-endspeed="1000"
data-endeasing="Power4.easeIn" style="z-index: 3; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
Texto Editable  Texto Editable  Texto Editable  Texto Editable  Texto Editable  Texto Editable  Texto Editable .
</div>

<div class="tp-caption customin ltl tp-resizeme"
data-x="center"
data-y="363"
data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
data-speed="800"
data-start="1550"
data-easing="easeOutQuad"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.01"
data-endelementdelay="0.1"
data-endspeed="1000"
data-endeasing="Power4.easeIn" style="z-index: 3;">
<a href="#purchase" class="btn btn-default btn-lg">
	<span>Texto Editable </span> 
</a>
</div>

</li>

<!-- SLIDE  -->
<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Slide 1" style="background-color: #F6F6F6;">

	<img src="../assets/images/backgrounds/grey-noise-min.jpg" alt="video" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat">

	<div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
	data-x="left" data-hoffset="0"
	data-y="70"
	data-customin="x:-200;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
	data-speed="400"
	data-start="1000"
	data-easing="easeOutQuad"
	data-splitin="none"
	data-splitout="none"
	data-elementdelay="0.01"
	data-endelementdelay="0.1"
	data-endspeed="1000"
	data-endeasing="Power4.easeIn">
	<img src="../assets/images/demo/desktop_slider_2.png" alt="">
</div>

<div class="tp-caption large_bold_grey skewfromrightshort customout font-open-sans"
data-x="645"
data-y="70"
data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="500"
data-start="800"
data-easing="Back.easeOut"
data-endspeed="500"
data-endeasing="Power4.easeIn"
data-captionhidden="off"
style="z-index: 4; font-weight:bold;">Texto Editable 
</div>

<div class="tp-caption large_bold_grey skewfromleftshort customout font-open-sans"
data-x="645"
data-y="133"
data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="300"
data-start="1100"
data-easing="Back.easeOut"
data-endspeed="500"
data-endeasing="Power4.easeIn"
data-captionhidden="off"
style="z-index: 7; font-weight:bold !important;">Texto Editable 
</div>

<div class="tp-caption ltl customin customout small_light_white font-lato"
data-x="650"
data-y="235"
data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
data-speed="500"
data-start="1300"
data-easing="easeOutQuad"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.01"
data-endelementdelay="0.1"
data-endspeed="500"
data-endeasing="Power4.easeIn" style=" color: #333; font-size:20px; max-width: 550px; white-space: normal; text-shadow:none;">
Texto Editable  Texto Editable  Texto Editable 
Texto Editable  Texto Editable  Texto Editable <br />
<strong>Texto Editable </strong>
</div>


<div class="tp-caption large_bold_darkblue skewfromleftshort customout"
data-x="650" data-hoffset="-90"
data-y="370"
data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="500"
data-start="1600"
data-easing="Back.easeOut"
data-endspeed="500"
data-endeasing="Power4.easeIn"
data-captionhidden="off"
style="z-index: 10; text-shadow:none;">Edi 
</div>

<div class="tp-caption medium_bg_darkblue skewfromleftshort customout"
data-x="650" data-hoffset="-90"
data-y="430"
data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="500"
data-start="1650"
data-easing="Back.easeOut"
data-endspeed="500"
data-endeasing="Power4.easeIn"
data-captionhidden="off"
style="z-index: 11; text-shadow:none;">Texto Editable 
</div>

<div class="tp-caption medium_bold_red skewfromleftshort customout start font300"
data-x="800" data-hoffset="-90"
data-y="390"
data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="500"
data-start="1850"
data-easing="Back.easeOut"
data-endspeed="500"
data-endeasing="Power4.easeIn"
data-captionhidden="off"
style="z-index: 13; text-shadow:none; font-weight:300;">Texto  <strong>Editable</strong>
</div>

<div class="tp-caption medium_bg_red skewfromleftshort customout"
data-x="800" data-hoffset="-90"
data-y="430"
data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="500"
data-start="2300"
data-easing="Back.easeOut"
data-endspeed="500"
data-endeasing="Power4.easeIn"
data-captionhidden="off"
style="z-index: 21; text-shadow:none;">Admin Included
</div>

</li>




</ul>

<div class="tp-bannertimer"><!-- progress bar --></div>
</div>
</div>
<!-- /REVOLUTION SLIDER -->


<!-- INFO BAR -->
<section class="info-bar info-bar-clean">
	<div class="container">

		<div class="row">

			<div class="col-sm-3">
				<i class="fa fa-money"></i>
				<h3>COTIZA TU AUTO</h3>
				<p>Solicita una cotización, eligiendo el modelo de tu preferencia.</p>
			</div>

			<div class="col-sm-3">
				<i class="fa fa-check-circle"></i>
				<h3>CITA DE MANEJO</h3>
				 <p>Entra a éste enlace para agendar tu cita de manejo.</p>
			</div>

			<div class="col-sm-3">
				<i class="fa fa-gears"></i>
				<h3>CITA DE SERVICIO</h3>
				 <p>Entra aquí para fijar una cita de servicio para tu auto.</p>
			</div>

			<div class="col-sm-3">
				<i class="fa fa-wrench"></i>
				<h3>REFACCIONES</h3>
				 <p>Entra para solicitar una cotización de refacciones.</p>
			</div>

		</div>

	</div>
</section>
<!-- /INFO BAR -->



<!-- VEHICULOS -->
<section>
	<div class="container">

		<h2 class="owl-featured noborder"><strong>NUESTROS </strong> MODELOS</h2>
		<div class="owl-carousel featured nomargin owl-padding-10" data-plugin-options='{"loop":true,"singleItem": false, "items": "4", "stopOnHover":false, "autoPlay":4000, "autoHeight": false, "navigation": true, "pagination": false}'>

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->

			<!-- modelo 1 -->
			<div class="shop-item nomargin">

				<div class="thumbnail">
					<!-- product image(s) -->
					<a class="shop-item-image" href="shop-single-left.html">
						<img class="img-responsive" src="../assets/images/autos/originales/avanza2018.jpg" alt="shop first image" />
						<img class="img-responsive" src="../assets/images/autos/originales/corolla2018.jpg" alt="shop first image" />
					</a>
					<!-- /product image(s) -->

				</div>

				<div class="shop-item-summary text-center">
					<h2>Avanza  - 2019</h2>

				</div>

				<!-- buttons -->
				<div class="shop-item-buttons text-center">
					<a class="btn btn-default" href="#"><i class="fa fa-cart-plus"></i>Cotizar</a>
				</div>
				<!-- /buttons -->
			</div>
			<!-- /modelo 1 -->


		</div>
	</div>
</section>
<!-- /VEHICULOS -->




<!-- -->
<section class="alternate">
	<div class="container">
		
		<div class="row">

			<div class="col-md-12">

				<div class="box-icon box-icon-side box-icon-color box-icon-round">
					 
					
					 
					<div class="shop-item-buttons text-center">
						<a class="box-icon-title" href="#">
						<h2>CONTACTANOS</h2>
					</a>
					<a class="btn btn-default" href="#"><i class="fa fa-user"></i>Contactar</a>
				</div>
					 
				</div>

			</div>

			 

			 

		</div>

	</div>
</section>
<!-- / -->

 	  			
				<!-- FOOTER -->
				<footer id="footer">
					<div class="container">

						<div class="row">
 
							<div class="col-md-12">
								<!-- Social Icons -->
								<div class="margin-top-20">
									<a href="#" class="social-icon social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">

										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>

									<a href="#" class="social-icon social-icon-border social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="Twitter">
										<i class="icon-twitter"></i>
										<i class="icon-twitter"></i>
									</a>

									<a href="#" class="social-icon social-icon-border social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">
										<i class="icon-gplus"></i>
										<i class="icon-gplus"></i>
									</a>

									<a href="#" class="social-icon social-icon-border social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
										<i class="icon-linkedin"></i>
										<i class="icon-linkedin"></i>
									</a>

									<a href="#" class="social-icon social-icon-border social-rss pull-left" data-toggle="tooltip" data-placement="top" title="Rss">
										<i class="icon-rss"></i>
										<i class="icon-rss"></i>
									</a>

								</div>
								<!-- /Social Icons -->

							</div>
						</div>
					</div>

					<div class="copyright">


						<div class="container">
							<ul class="pull-right nomargin list-inline mobile-block">
								<li><a href="#">Terminos y Condiciones</a></li>
								<li>&bull;</li>
								<li><a href="#">Privacy</a></li>
							</ul>
							&reg; Todos los derechos reservados.
						</div>


					</div>
				</footer>
				<!-- /FOOTER -->

			</div>
			<!-- /wrapper -->


		<!-- 
			SIDE PANEL 
			
				sidepanel-dark 			= dark color
				sidepanel-light			= light color (white)
				sidepanel-theme-color		= theme color
				
				sidepanel-inverse		= By default, sidepanel is placed on right (left for RTL)
								If you add "sidepanel-inverse", will be placed on left side (right on RTL).
							-->
							<div id="sidepanel" class="sidepanel-light">
								<a id="sidepanel_close" href="#"><!-- close -->
									<i class="glyphicon glyphicon-remove"></i>
								</a>

								<div class="sidepanel-content">
									<h2 class="sidepanel-title">Explore Smarty</h2>

									<!-- SIDE NAV -->
									<ul class="list-group">

										<li class="list-group-item">
											<a href="#">
												<i class="ico-category et-heart"></i>  
												ABOUT US
											</a>
										</li>
										<li class="list-group-item list-toggle"><!-- add "active" to stay open on page load -->
											<a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-1" class="collapsed"> 
												<i class="ico-dd icon-angle-down"><!-- Drop Down Indicator --></i>
												<i class="ico-category et-strategy"></i>
												PORTFOLIO
											</a>
											<ul id="collapse-1" class="list-unstyled collapse"><!-- add "in" to stay open on page load -->
												<li><a href="#"><i class="fa fa-angle-right"></i> 1 COLUMN</a></li>
												<li class="active">
													<span class="badge">New</span>
													<a href="#"><i class="fa fa-angle-right"></i> 2 COLUMNS</a>
												</li>
												<li><a href="#"><i class="fa fa-angle-right"></i> 3 COLUMNS</a></li>
											</ul>
										</li>
										<li class="list-group-item list-toggle"><!-- add "active" to stay open on page load -->                
											<a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-2" class="collapsed"> 
												<i class="ico-dd icon-angle-down"><!-- Drop Down Indicator --></i>
												<i class="ico-category et-trophy"></i>
												PORTFOLIO
											</a>
											<ul id="collapse-2" class="list-unstyled collapse"><!-- add "in" to stay open on page load -->
												<li><a href="#"><i class="fa fa-angle-right"></i> SLIDER</a></li>
												<li class="active"><a href="#"><i class="fa fa-angle-right"></i> HEADERS</a></li>
												<li><a href="#"><i class="fa fa-angle-right"></i> FOOTERS</a></li>
											</ul>
										</li>
										<li class="list-group-item">
											<a href="#">
												<i class="ico-category et-happy"></i>  
												BLOG
											</a>
										</li>
										<li class="list-group-item">
											<a href="#">
												<i class="ico-category et-beaker"></i> 
												FEATURES
											</a>
										</li>
										<li class="list-group-item">
											<a href="#">
												<i class="ico-category et-map-pin"></i> 
												CONTACT
											</a>
										</li>

									</ul>
									<!-- /SIDE NAV -->

									<!-- social icons -->
									<div class="text-center margin-bottom-30">

										<a href="#" class="social-icon social-icon-sm social-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
											<i class="icon-facebook"></i>
											<i class="icon-facebook"></i>
										</a>

										<a href="#" class="social-icon social-icon-sm social-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
											<i class="icon-twitter"></i>
											<i class="icon-twitter"></i>
										</a>

										<a href="#" class="social-icon social-icon-sm social-linkedin" data-toggle="tooltip" data-placement="top" title="Linkedin">
											<i class="icon-linkedin"></i>
											<i class="icon-linkedin"></i>
										</a>

										<a href="#" class="social-icon social-icon-sm social-rss" data-toggle="tooltip" data-placement="top" title="RSS">
											<i class="icon-rss"></i>
											<i class="icon-rss"></i>
										</a>

									</div>
									<!-- /social icons -->

								</div>

							</div>
							<!-- /SIDE PANEL -->

							<!-- SCROLL TO TOP -->
							<a href="#" id="toTop"></a>

							<!-- JAVASCRIPT FILES -->
							<script type="text/javascript">var plugin_path = '../assets/plugins/';</script>
							<script type="text/javascript" src="../assets/plugins/jquery/jquery-2.1.4.min.js"></script>

							<script type="text/javascript" src="../assets/js/scripts.js"></script>
							
							
							<!-- REVOLUTION SLIDER -->
							<script type="text/javascript" src="../assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
							<script type="text/javascript" src="../assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
							<script type="text/javascript" src="../assets/js/view/demo.revolution_slider.js"></script>

						</body>
						</html>